function addSlotsToText(text, result) {
    var textWithEmotions = addEmotionsToText(text, result.emotions, result.emotionParties, result.emotionTexts);
    return addChairsToText(textWithEmotions, result.chairTexts, result.chairSpeaker);
}

function textWithoutAnyAnchor(text) {
    return text.replace(/###E###/g, "").replace(/###C###/g, "").replace(/#/g, "")
}

function startOfTextWithoutAnyAnchor(text) {
    if (text.startsWith('#'))
        return 1 + startOfTextWithoutAnyAnchor(text.slice(1));
    else if (text.startsWith('E##'))
        return 3 + startOfTextWithoutAnyAnchor(text.slice(3));
    else if (text.startsWith('C##'))
        return 3 + startOfTextWithoutAnyAnchor(text.slice(3));
    else
        return 0;
}

function endOfTextWithoutAnyAnchor(text) {
    if (text.endsWith('#'))
        return endOfTextWithoutAnyAnchor(text.slice(0, text.length - 1));
    else if (text.endsWith('##E'))
        return endOfTextWithoutAnyAnchor(text.slice(0, text.length - 3));
    else if (text.endsWith('##C'))
        return endOfTextWithoutAnyAnchor(text.slice(0, text.length - 3));
    else
        return text.length;
}