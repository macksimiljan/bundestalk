/**
	Handling of style related actions. 
	Complete dropdown menus, toggle, fill up divider of the header
*/

//var urlSuggest = "media/Parteien.json";
var urlSuggest = 'webservice/suggest';//(window.location.host.indexOf("localhost") !== -1) ? "" : "/webapp" + "/webservice/suggest";

$(document).ready(function() {
	
	//fillng divider for header
	fillDivider();
	
	//filling the election period dropdown menu
	var selectPicker = document.getElementById("election-period");
	selectPicker.options.add(new Option("alle Wahlperioden", 0, true));
	for(var i=17; i < 20; i++){
		selectPicker.options.add(new Option("Wahlperiode "+i, i, false));
	}
	$("#election-period").selectpicker("refresh");

	//filling the party dropdown
	fillParty('search-party');

	// filling the emotion dropdown and the party of the emotion
	fillEmotion();
	fillParty('search-emotion-party');
	
	//Datepicker
	var options = {autoclose: true, format: "dd.mm.yyyy", clearBtn: true, language: "de-De"};
	$("#year-from").datepicker(options).on("changeDate", function(e){
		adjustDate("#year-to",e.date,true);
	}).on("clearDate", function(e){
		adjustDate("#year-to",e.date,false);
	});
	$("#year-to").datepicker(options).on("changeDate", function(e){
		adjustDate("#year-from",e.date,true);
	}).on("clearDate", function(e){
			adjustDate("#year-from",e.date,false);
	});

	var options = {
	
		url: function(phrase){
			return urlSuggest + "?type=speaker&value=" + phrase;
		},
		getValue: "name",
		list: {
			match: {
				enabled: true
			},
                        onChooseEvent: function(){
                            var selectedItem = $("#search-politician").getSelectedItemData();
                            if(selectedItem && selectedItem !== -1){
                                $("#search-politician-fp").val(selectedItem.fingerprint);
                                $("#search-politician-fp").data("politician-name", selectedItem.name.trim())
                            } else {
                                $("#search-politician-fp").val("");
                                $("#search-politician-fp").data("politician-name", "");
                            }
                           
                        }
		},
		listLocation: "data",
		adjustWidth: false,
		template: {
			type: "iconRight",
			fields: {
				iconSrc: "party"
				}
			}
	};
	$("#search-politician").easyAutocomplete(options);



	var queryAutoCompleteOptions = {
	
		url: function(phrase){
			return urlSuggest + "?type=query&value=" + phrase;
		},
		getValue: "key",
		list: {
			match: {
				enabled: true
			},
			onChooseEvent: function(){
				
			}
		},
		listLocation: "data",
		adjustWidth: false
	};
	$("#search-query").easyAutocomplete(queryAutoCompleteOptions);
	
});

function adjustDate(origin, date, isChange){ 
	var fnName = (origin === "#year-from") ? "setEndDate" : "setStartDate",
		origin = $(origin);
	if(fnName === "setEndDate" && isChange && origin.datepicker("getDate") > date && origin.datepicker("getDate")){
		origin.datepicker("setDate", date);
	}
	if(fnName === "setStartDate" && isChange && origin.datepicker("getDate") < date && origin.datepicker("getDate")){
		origin.datepicker("setDate", date);
	}
	origin.datepicker(fnName, date);
	}
//Toggle for advanced search options
Visible = {visible: false};
var toggleAdvancedSearch = function(){
	if(Visible.visible){
		Visible.visible= false;
		setTimeout(function(){
		$(".toggle-icon").remove();
		$(".advanced-search").append("<i class=\"fa fa-chevron-down toggle-icon\"</i>");
		$("#search-options").addClass("hidden");
		});
		
	}else{
		Visible.visible = true;
		setTimeout(function(){
		$(".toggle-icon").remove();
		$(".advanced-search").append("<i class=\"fa fa-chevron-up toggle-icon\"</i>");
		$("#search-options").removeClass("hidden");
		});
	}
};
//filling divider with images
function fillDivider(){
	var imageDivider = $("#divider-header");
	var imageParty = "<div class=\"partei-img\"><img src=\"media/parteien/linke.png\"></div><div class=\"partei-img\"><img src=\"media/parteien/gruene.png\"></div><div class=\"partei-img\"><img src=\"media/parteien/spd.png\"></div><div class=\"partei-img\"><img src=\"media/parteien/cdu.png\"></div><div class=\"partei-img\"><img src=\"media/parteien/csu.png\"></div><div class=\"partei-img\"><img src=\"media/parteien/fdp.png\"></div><div class=\"partei-img\"><img src=\"media/parteien/afd.png\"></div>";
	//console.log(window.innerWidth);
	for(var i = 0; i < (window.innerWidth / 390); i++){
		imageDivider.append(imageParty);
	}
}

function fillParty(fieldId){
	
		$.ajax({
		url: urlSuggest + "?type=party",
		method:'GET',
		success: function(response){
			addData(response.data, fieldId);
		},
		error: function(response) {
		}
	});
	
}

function fillEmotion() {
	$.ajax({
		url: 'webservice/suggest?type=emotion',
		method:'GET',
		header: [{'Content-type': 'application/json'}],
		success: function (response) {
			addEmotion(response);
        }
	})
}


function addData(data, id){
	var selectPicker = document.getElementById(id);
	for(var i = 0; i < data.length; i++){
		var opt = new Option(data[i].name, data[i].slug, false);
		// console.log(data[i].name);
		opt.setAttribute("data-content", "<span><img class=\"option-img\" src=\"media/parteien/"+data[i].slug + ".png\"> " + data[i].name + "</span>");		
		selectPicker.options.add(opt);
		
	}
	$("#" + id).selectpicker("refresh");
}

function addEmotion(response) {
	var selectPicker = document.getElementById('search-emotion');
	for (var i = 0; i < response.count; i++) {
		var emotion = response.data[i];
		var opt = new Option(emotion, emotion, false);
		opt.setAttribute('data-content', "<span><span class='emoticon'>" + chooseEmoticon(emotion) + "</span>" + emotion + "</span>");
		selectPicker.options.add(opt);
	}
	$("#search-emotion").selectpicker('refresh');
}
//reset values of advanced search
function resetAdvancedSearch(){
	$("#search-topic").val("");
	$("#search-emotion").selectpicker('deselectAll');
	$("#search-emotion-party").selectpicker('deselectAll');
	$("#search-politician").val("");
	$("#search-party").selectpicker('deselectAll');
	$("#election-period").val(0);
	$("#election-period").selectpicker('refresh')
	$("#year-from").datepicker("clearDates");
	$("#year-to").datepicker("clearDates");	
}

