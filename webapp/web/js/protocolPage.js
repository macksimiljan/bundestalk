$(document).ready(function() {
	 //get whole protocol of one session
	   getProtocol();
	
});

function getProtocol(){

    function getQueryVariable(variable){
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
              var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
            }
            return(false);
     }
     
    var id = getQueryVariable("id").trim();
    var wp = getQueryVariable("wp").trim();
    var s = getQueryVariable("s").trim();

    $.ajax({
            url: "htmlProtocol/" + wp + "_" + s + ".html",
            crossOrigin: true,
            method: 'GET',
            success: function(response){
                    //console.log(response);
                    //$("#page-loader").hide((function(){
                    $("#page-loader").hide();
                            $("#whole-speech").show();
                            $("#whole-speech").append(response);
                            $("div[speech-id=" + id + "]").addClass("speech-focus").attr("id", "speech-focus");    
                    	    console.log($("div[speech-id=" + id + "]"));
                            console.log($("#speech-focus"));
                            $("html,body").animate({scrollTop: $("#speech-focus").offset().top - 100});
			
                   // });

            },
            error: function onError(request, status, error) {
                    console.log(request.responseText);
            }
    });

}
