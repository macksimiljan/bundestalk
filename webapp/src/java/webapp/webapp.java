/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webapp;

import java.io.IOException;
import java.io.PrintWriter;
import groupa.searcher;
import groupa.Suggester;
import groupa.Speaker;
import groupa.Party;
import groupa.searchResult;
import groupa.AutoCompleter;
import java.util.*;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.time.LocalDateTime;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import org.apache.lucene.search.suggest.Lookup;

/**
 *
 * @author falk
 */

@WebServlet(description = "Webservice", urlPatterns = { "/webservice/*" , "/webservice.do"}, initParams = {@WebInitParam(name="id",value="1"),@WebInitParam(name="name",value="test")})
public class webapp extends HttpServlet {
    
    Properties prop;
    Writer logwriter;
    AutoCompleter autocompleter;
    
    private static final String[] logfields = {"time","user","text","speaker","speaker_fp","party","bTop","wp","s","emotion","emotion_party"};
    
    public void init(ServletConfig config)throws ServletException {
        super.init(config);
         try {
            prop = new Properties();
            ServletContext ctx = getServletContext();
            prop.load(ctx.getResourceAsStream("/WEB-INF/global.properties"));
            
            File f = new File(getServletContext().getRealPath("/WEB-INF/local.properties"));
            if (f.exists()){
                prop.load(getServletContext().getResourceAsStream("/WEB-INF/local.properties"));
            }
            
            autocompleter = new AutoCompleter();
            autocompleter.loadLogfile(prop.getProperty("logFile", "/var/log/bundestalk/requests.log"),Integer.parseInt(prop.getProperty("autocompleteReloadIntervall", "0")));

            
         } catch (Exception e) {
             System.out.print(e);
             e.printStackTrace();
        }
    }
    
    /**
    * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
    */
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
           doGet(request, response);
   }
    
    /**
    * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
    */
   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
           response.setContentType("application/json");
           response.setCharacterEncoding("UTF-8");
           response.setHeader("Access-Control-Allow-Origin", "*");

           PrintWriter out = response.getWriter();
           if(request.getPathInfo().equals("/search")){
               seach(out, request, response);
           }
           else if(request.getPathInfo().equals("/suggest")){
               suggest(out, request, response);
           }
            else {
               out.println("{\"message\": \"no action set " + request.getPathInfo() + "\"}");
           }
           
           
           out.flush();
   }

   public boolean isNumeric(String str)
    {
      return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
   
   protected void suggest(PrintWriter out, HttpServletRequest request, HttpServletResponse response){
       webserviceResponse res = new webserviceResponse();
       
       String type = "party";
       
       if(request.getParameter("type") != null){
            type = request.getParameter("type");
       }
       
       String value = "";
       if(request.getParameter("value") != null){
            value = request.getParameter("value");
       }
       
       String SqLiteDbFile = getServletContext().getRealPath(prop.getProperty("dbFile", "/WEB-INF/db.sqlite"));
       
       try {
            if(type.equals("party")){
                    Suggester sug = new Suggester(SqLiteDbFile);

                    ArrayList<Party> l = sug.SearchParties(value);
                    res.data = l.toArray(new Party[l.size()]);
                    res.count = l.size();

            }
            else if(type.equals("speaker")){
                    Suggester sug = new Suggester(SqLiteDbFile);

                    ArrayList<Speaker> l = sug.searchSpeaker(value);
                    res.data = l.toArray(new Speaker[l.size()]);
                    res.count = l.size();

            }
            else if(type.equals("emotion")){
                    Suggester sug = new Suggester(SqLiteDbFile);

                    List<String> emotions = sug.searchEmotion(value);
                    res.data = emotions.toArray(new String[emotions.size()]);
                    res.count = emotions.size();

            }else if(type.equals("query")){
                List<Lookup.LookupResult> completions = autocompleter.suggest(value,5);
                res.data = completions;
                res.count = completions.size();
            }
            
        } catch (Exception e) {
            res.success = false;
            res.message = "ERROR: " + e.getMessage();
            e.printStackTrace();
        }

       try {
            ObjectMapper mapper = new ObjectMapper();
            out.println(mapper.writeValueAsString(res));
       } catch (Exception e) {
            out.println("JSON-ERROR");
       }  
   }  
   
   private String maskInput(String value){
       return value.replace("\"", "\\\"").replace("-", "\\-").replace("+", "\\+").replace(":", "\\:").replace("\\", "\\\\").replace("!", "\\!").replace("|", "\\|").replace("&", "\\&");
   }
   
   protected void seach(PrintWriter out, HttpServletRequest request, HttpServletResponse response){
       webserviceResponse res = new webserviceResponse();
       Integer limit = 10;
       Integer offset = 0;
       
       if(request.getParameter("offset") != null && isNumeric(request.getParameter("offset"))){
            offset = Integer.parseInt(request.getParameter("offset"));
       }
       
       if(request.getParameter("limit") != null && isNumeric(request.getParameter("limit"))){
            limit = Integer.parseInt(request.getParameter("limit"));
       }
       
        HashMap<String, String> values = new HashMap<String, String>();
           if(request.getParameter("value") != null && !request.getParameter("value").isEmpty()){
               values.put("text", request.getParameter("value"));
           }
           
           if(request.getParameter("party") != null && !request.getParameter("party").isEmpty()){
               values.put("party", request.getParameter("party"));
           }
           
           if(request.getParameter("politician") != null && !request.getParameter("politician").isEmpty()){
               values.put("speaker", request.getParameter("politician"));
           }
           
           if(request.getParameter("politician-fp") != null && !request.getParameter("politician-fp").isEmpty()){
               values.put("speaker_fp", request.getParameter("politician-fp"));
           }
           
           if(request.getParameter("topic") != null && !request.getParameter("topic").isEmpty()){
               values.put("bTop", request.getParameter("topic"));
           }
           
           if(request.getParameter("wp") != null && !request.getParameter("wp").isEmpty()){
               values.put("wp", request.getParameter("wp"));
           }
           
           if(request.getParameter("s") != null && !request.getParameter("s").isEmpty()){
               values.put("s", request.getParameter("s"));
           }
           
           if(request.getParameter("timestamp") != null && !request.getParameter("timestamp").isEmpty()){
               values.put("timestamp", request.getParameter("timestamp"));
           }

           if (request.getParameter("emotion") != null && !request.getParameter("emotion").isEmpty()) {
             values.put("emotion", request.getParameter("emotion"));
           }

           if (request.getParameter("emotion_party") != null && !request.getParameter("emotion_party").isEmpty()) {
             values.put("emotion_party", request.getParameter("emotion_party"));
           }
            if (request.getParameter("sort") != null && !request.getParameter("sort").isEmpty()) {
             values.put("sort", request.getParameter("sort"));
           }
       
       try {    
           searcher s = new searcher(getServletContext().getRealPath(prop.getProperty("indexPath", "/WEB-INF/index")), getServletContext().getRealPath(prop.getProperty("dbFile", "/WEB-INF/db.sqlite")));
           
           searchResult sResult = s.boolSearch(values, offset, limit);
           res.data =  sResult.docs;
           res.count = sResult.count;
           res.taxo = sResult.taxo;
           
           QuerySuggester sug = new QuerySuggester(getServletContext().getRealPath(prop.getProperty("dbFile", "/WEB-INF/db.sqlite")));
           QuerySuggester.Query originalQuery = new QuerySuggester.Query(request.getParameter("value"),request.getParameter("politician"),request.getParameter("party"),request.getParameter("emotion"));
           res.alternativeQuery = sug.suggest(originalQuery);
           
           //Add some values to the request for logging. DO NOT PASS THIS VALUES TO THE SEARCHER!
           values.put("time", LocalDateTime.now().toString());
           values.put("user", request.getRemoteAddr());
           logRequest(values);
           
       } catch (Exception e) {
           res.success = false;
           res.message = "ERROR: " + e.getMessage();
           e.printStackTrace();
       }
       
       try {
            ObjectMapper mapper = new ObjectMapper();
            out.println(mapper.writeValueAsString(res));
       } catch (Exception e) {
            out.println("JSON-ERROR");
            e.printStackTrace();
       }  
   }

   public void logRequest(HashMap<String, String> values) throws IOException{
       
       if (logwriter == null){
           String filename = prop.getProperty("logFile", "");
           if(filename.equals("")){
               //No logfile means logging is disabled
               return;
           }
           File logfile = new File(filename);
           logfile.getParentFile().mkdirs();
           logfile.createNewFile();
           logwriter = new FileWriter(logfile,true);
       }
       
       
       boolean first = true;
       String line = "";
       for (String field : logfields){
           String value = values.get(field);
           if (value == null){
               value = "";
           }
           if (!first){
               line += ", ";
           }
           line += value;
           
           first = false;
       }
       logwriter.append(line+"\n");
       logwriter.flush();
       
   }
}
	
