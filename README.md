# Group A: Bundestagsreden

## Struktur

### crawler

- Crawlt Bundestagsseite und lädt txt-Protokolle herunter
- extrahiert Daten aus txt Protokollen
- speichert Redebeiträge und Tagesordnungspunkte in SqLite-Datenbank

### indexer

- indexiert sqLite Daten
- Commands: 
    - komplett: cd indexer/dist && java -jar groupA.jar all ../../data/db/db.sqlite ../../data/index
    - nur index bilden: cd indexer/dist && java -jar groupA.jar indexing ../../data/db/db.sqlite ../../data/index
    - nur suggest bilden: cd indexer/dist && java -jar groupA.jar suggester ../../data/db/db.sqlite

## Webapp kompilieren
Um den index zu erstellen wie folgt vorgehen:
1. Docker-Cotainer bauen (mindestens einmal nötig. Danach nur noch, wenn sich was an der Build-Umgebung geändert hat):
```
docker build -t builder .
```

Dieser container enthält dann python, java, make und diverse Abhängigkeiten, aber nicht den eigentlichen Code. Der wird beim Ausführen des Containers nach /src eingeblendet.

2. Container starten
```
docker run -it --rm -v <projectverzeichnis>:/src builder <command>
```
**\<projectverzeichnis\>** muss durch den ABSOLUTEN Pfad ersetzt werden, in dem die Projekt liegt.

ACHTUNG! Hier wird nicht mehr das data-verzeichnis gewollt, sondern das komplette Projekt-Verzeichnis. Das hat den Vorteil, dass der Container nicht nach jeder Änderung am Quellcode neu gebaut werden muss. Das sollte den Entwicklungsprozess deutlich bequemer gestalten.

**\<command\>** gibt an, was genau getan werden soll. Folgende Optionen sind möglich:  
- test (lässt die Testfälle durchlaufen)
- scrape (lässt den parser laufen und erstellt DB)
- index (erstellt aus der DB den index)
- webapp (erstellt eine war-datei der Webapp und legt sie im gleichen ordenr wie die db und den index ab)
- clean (löscht alle java-buildartefakte. Wenn irgendwas nicht klappt, erstmal clean und dann nochmal versuchen)
- ohne angabe wird webapp aufgeführt

Im Gegensatz zu früher müssen nicht alle Kommandos manuell nacheinander ausgeführt werden. Das Build-System weiß, was es tun muss um ein bestimmtes kommando auszuführen. Soll als der index erstellt werden, wird bei bedarf zuvor gescraped.  
Dabei wird immer nur das gemacht was nötig ist. Hat sich z.B. die db und der code des indexers seit dem letzten durchlauf nicht geändert wird der index nicht neu erzeugt.

## Webapp ausführen
Lege die zuvor erstellte war-datei in ein ansonsten leeres verzeichnis und nenn sie ROOT.war (wenn sie anderst heißt, z.B. bla.war ist die webseite später unter domain/bla zu finden).  
Starte einen Tomcat-Server mittels docker und mounte da verzeichnis mit der war-datei an die richtige Stelle. Wenn die war-datei im aktuellen Ordner liegt:  
```
docker run --rm -it -v `pwd`:/usr/local/tomcat/webapps -v /var/log/bundestalk:/var/log/bundestalk -p 8080:8080 tomcat
```
Anschließend ist die Webseite unter http://localhost:8080 erreichbar.
Wenn das Verzeichnis /var/log/bundestalk nicht wie beschrieben gemounted wird, verschwinden die query-logs (und damit auch die autocomplete-suggestions) beim Neustart des Containers.


## webapp
- config
    - webapp/WEB-INF/global.properties
        - enthält globale Konfiguration
        - zum Beispiel der Order, wo der Index liext
    - webapp/WEB-INF/local.properties
        - enthält lokale Konfiguration
        - diese Datei ist optional und aus dem Repo ausgeschlossen
        - wenn man zum beispiel Pfade lokal anders hat als auf dem server, kann man die "global.properties" kopieren, umbenennen in "local.properties" und hier die Pfade dann anpassen
- java Servlet für Suche mit Rest-Api
    - webservice/search?
        - value=cdu
        - offset=0
        - limit=10
    - webservice/suggest?
        - type=party
        - value=s

## Reference

- Python Module
    - https://pypi.python.org/pypi/banal
    - https://pypi.python.org/pypi/certifi
    - https://pypi.python.org/pypi/chardet
    - https://pypi.python.org/pypi/normality
    - https://pypi.python.org/pypi/requests
    - https://pypi.python.org/pypi/urllib3
    - https://github.com/python/cpython/
    - https://github.com/bundestag/normdatei