# coding: utf-8
import os;
import sys;
import re;
import locale;
from io import open;
from datetime import datetime;

from normdatei.text import clean_text, clean_name, fingerprint;
from normdatei.parties import search_party_names;

import groupA.dateparser

BEGIN_MARK = re.compile('Beginn:? [X\d]{1,2}.\d{1,2} Uhr')

# speaker types
POI_PARTY_MEMBER = re.compile('\s*(.{5,140}\(.*\))\s*$')
POI_PRESIDENT = re.compile('\s*((Alterspräsident(?:in)?|Vizepräsident(?:in)?|Pr\xe4sident(?:in)?|Präsident(?:in)?).{5,140})\s*$')
POI_STAATSSEKR = re.compile('\s*(.{5,140}, Parl\. Staatssekretär(in)?\s{1,5}[A-Z]{2,10}.*)$')
POI_STAATSMINISTER = re.compile('\s*(.{5,140}, Staatsminister(in)?\s{1,5}[A-Z]{2,10}.*)$')
POI_MINISTER = re.compile('\s*(.{5,140}, Bundesminister(in)?\s{1,5}[A-Z]{2,10}.*)$')
POI_WEHRBEAUFTRAGTER = re.compile('\s*(.{5,140}, Wehrbeauftragter.*)\s$')
POI_BUNDESKANZLER = re.compile('\s*(.{5,140}, Bundeskanzler.*)\s$')
POI_BEAUFTRAGT = re.compile('\s*(.{5,140}, Beauftragter? der Bundes.*)\s$')


#Topparser
POI_START_MARK = re.compile('Inhalt:( )?|I n h a l t ?:|Inhalt\s*$')
POI_Sub_Mark = re.compile('^(Zusatztagesordnungspunkt|Tagesordnungspunkt) (\d{1,3}|[I,V,X]{1,3})( \(Fortsetzung\))?:')

PAGE_REFERENCE = re.compile('^\s*\d{1,6} [A,B,C,D]\s*$')
ATTACHMENT = re.compile('^\s*Anlage \d{1,3}\s*')
NEXT_SESSION = re.compile("^\s*Nächste Sitzung\s*$")
DATE_FORMAT = re.compile("^\w+, \w+, den (.+)$")


# WP is not always give inside the text, so wo need to get it from the filename
def file_metadata(filename):
    fname = os.path.basename(filename)
    return int(fname[:2]), int(fname[2:5])

# some protocolls contain strange unicode-chars (dots?). Remove them as they break the regexes!
def clean_strange_unicode_char(line):
    return line.replace("\u200e","")


class TopParser(object):
    
    def __init__(self, lines):
        self.lines = lines
        self.lineiter = iter(lines)
        self.topic = ""
        self.speakers = []
        self.currentline = clean_strange_unicode_char(self.lineiter.__next__())
        self.nextline = clean_strange_unicode_char(self.lineiter.__next__())
        self.topics = []
        self.date = None
        self.topseqnr = 0
        self.eof = False

    def emit(self):
            data = {
                'topic': self.topic,
                'speakers': self.speakers,
                'id': self.topseqnr
            }

            if not self.topic:
                print("Found an empty topic. This should be investigated!")

            self.topic = ""
            self.speakers = []
            self.topseqnr += 1
            self.topics.append(data)

    def next_line(self):
        try:
            self.currentline = self.nextline
            self.nextline = clean_strange_unicode_char(self.lineiter.__next__())
        except:
            self.currentline = ""
            self.nextline = ""
            self.eof = True
            

    def match_eof(self):
        return self.eof

    def match_trash(self):
        self.next_line()
        return True

    def match_date(self):
        fixedline = self.currentline.replace("\u200e","").strip()
        datematch = DATE_FORMAT.match(fixedline)
        if datematch:
            try:
                self.date = groupA.dateparser.parse(datematch.group(1))
            except ValueError as e:
                print("Failed to parse Date:",repr(fixedline),e)
                return False

            self.next_line()
            return True
        return False

    def match_topic(self):
        if not POI_Sub_Mark.match(self.currentline) and not ATTACHMENT.match(self.currentline) and not BEGIN_MARK.match(self.currentline) and not NEXT_SESSION.match(self.currentline):
            self.topic += self.currentline +"\n"
            self.next_line()
            return True
        return False

    def match_fragestunde_trash(self):
        if not POI_Sub_Mark.match(self.currentline) and not ATTACHMENT.match(self.currentline) and not BEGIN_MARK.match(self.currentline) and not NEXT_SESSION.match(self.currentline):
            self.next_line()
            return True
        return False

    def match_newline(self):
        if not self.currentline:
            return False

        if not self.currentline.strip():
            self.next_line()
            return True
        return False

    def match_pageref(self):
        if PAGE_REFERENCE.match(self.currentline):
            self.next_line()
            return True
        return False

    def match_startmark(self):
        if POI_START_MARK.match(self.currentline):
            self.next_line()
            return True
        return False

    def match_endmark(self):
        if BEGIN_MARK.match(self.currentline):
            self.next_line()
            return True
        return False

    def match_speaker(self):
        speakermatch =  (POI_PRESIDENT.match(self.currentline) or
                    POI_PARTY_MEMBER.match(self.currentline) or
                    POI_STAATSSEKR.match(self.currentline) or
                    POI_STAATSMINISTER.match(self.currentline) or
                    POI_WEHRBEAUFTRAGTER.match(self.currentline) or
                    POI_BUNDESKANZLER.match(self.currentline) or
                    POI_BEAUFTRAGT.match(self.currentline) or
                    POI_MINISTER.match(self.currentline))


        if speakermatch:
            # If the chairman appears as planned-speaker things break badly. Issue #47 
            # Just assume he does not hold speeches
            if not POI_PRESIDENT.match(self.currentline):
                self.speakers.append(speakermatch.group(1).strip())
            self.next_line()
            return True
        return False

    def match_multiline_speaker(self):
        speakermatch =  (POI_PRESIDENT.match(self.currentline+self.nextline) or
                    POI_PARTY_MEMBER.match(self.currentline+self.nextline) or
                    POI_STAATSSEKR.match(self.currentline+self.nextline) or
                    POI_STAATSMINISTER.match(self.currentline+self.nextline) or
                    POI_WEHRBEAUFTRAGTER.match(self.currentline+self.nextline) or
                    POI_BUNDESKANZLER.match(self.currentline+self.nextline) or
                    POI_BEAUFTRAGT.match(self.currentline+self.nextline) or
                    POI_MINISTER.match(self.currentline+self.nextline))
        if speakermatch:
            self.next_line()
            self.next_line()
            self.speakers.append(speakermatch.group(1).strip());
            return True
        return False

    def match_topicheading(self):
        if POI_Sub_Mark.match(self.currentline):
            self.next_line()
            return True
        return False

    def match_fragestunde_header(self):
        if "Fragestunde" in self.currentline:
            self.topic += "Fragestunde\n"
            self.next_line()
            return True
        return False

    def match_fragestunde(self):
        if self.match_fragestunde_header():
            self.match_topic()
            while self.match_fragestunde_trash():
                pass
            return True
        return False

    def match_top_body(self):
        if not self.match_topic():
            return False

        while self.match_pageref() or self.match_newline() or self.match_speaker() or self.match_multiline_speaker() or self.match_topic():
            pass
        return True


    def match_top(self):
        while self.match_newline():
            pass

        if not self.match_topicheading():
            return False

        if self.match_fragestunde() or self.match_top_body():
            self.emit()

        return True

        
    def parse(self):

        while not self.match_date():
            if self.match_eof():
                raise ValueError("Syntax error: Protocol does not contain a date-line!")
            self.match_trash()

        while not self.match_startmark():
            if self.match_eof():
                raise ValueError("Syntax error: Protocol does not contain a startmark!")
            self.match_trash()

        
        while not self.match_top():
            if self.match_eof():
                raise ValueError("Syntax error: Protocol does not contain at least one top!")
            self.match_trash()

        while self.match_top():
            pass

        return {
            "tops":self.topics,
            "date": self.date
        }
     
   

def parse_metadata(txt_file):
    if not os.path.exists(txt_file):
        print("file not found");
        sys.exit(0);
        
    text = "";
    try:
        with open(txt_file, encoding="UTF-8") as fh:
            content = fh.read()
            text = clean_text(content)
    except UnicodeDecodeError:
        #print("Reloading",txt_file,"in other encoding (windows-1252)")
        with open(txt_file, encoding="windows-1252") as fh:
            content = fh.read()
            text = clean_text(content)

    parser = TopParser(text.split('\n'))
    erg = parser.parse()

    if not erg:
        return False

    erg["filename"] = txt_file
    erg["url"] = ""
    wp, s = file_metadata(txt_file)
    erg["wp"] = wp
    erg["session"] = s
    
    return erg;

