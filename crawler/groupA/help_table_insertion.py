import sqlite3

from groupA.emotionparser import EMOTIONS


def data_for_speakers(connection):
    sql = '''
        SELECT
          s.speaker_fp as fingerprint,
          MIN(speaker_cleaned) name,
          MIN(speaker_party) party,
          n AS activity_measure
        FROM speak s, (
          SELECT
            speaker_fp,
            MIN(LENGTH(speaker_cleaned)) as name_length,
            MAX(LENGTH(speaker_party)) as party_length,
            COUNT(*) AS n
          FROM speak
          WHERE type = 'speech'
            AND LENGTH(speaker_fp) < 35
          GROUP BY speaker_fp
        ) AS t0
        WHERE t0.speaker_fp = s.speaker_fp
          AND (LENGTH(s.speaker_cleaned) = name_length OR name_length IS NULL)
          AND (LENGTH(s.speaker_party) = party_length OR party_length IS NULL)
        GROUP BY s.speaker_fp;'''

    cursor = connection.cursor()
    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    return rows


def create_emotion_table(connection):
    connection.execute('''DROP TABLE IF EXISTS emotions;''')
    connection.execute('''CREATE TABLE IF NOT EXISTS emotions (
                            name VARCHAR(25) PRIMARY KEY,
                            frequency INTEGER,
                            UNIQUE (name)
                            )''')


def create_speaker_table(connection):
    connection.execute('''DROP TABLE IF EXISTS speakers;''')
    connection.execute('''CREATE TABLE IF NOT EXISTS speakers (
                             fingerprint VARCHAR(40) PRIMARY KEY,
                             name VARCHAR(105),
                             party VARCHAR(30),
                             activity_measure INT
                             )''')


def insert_emotions(connection):
    cursor = connection.cursor()
    for emotion in EMOTIONS:
        cursor.execute('''INSERT OR IGNORE INTO emotions(name, frequency)
                            VALUES (?,?)''', (emotion["type"], emotion["frequency"]))
    connection.commit()
    cursor.close()


def insert_speakers(connection):
    cursor = connection.cursor()
    for politician in data_for_speakers(connection):
        cursor.execute('''INSERT OR IGNORE INTO speakers (fingerprint, name, party, activity_measure) VALUES (?, ?, ?, ?)''',
                  (politician[0], politician[1], politician[2], politician[3]))

    connection.commit()
    cursor.close()


def insert_help_tables(database_uri):
    connection = sqlite3.connect(database_uri)

    create_speaker_table(connection)
    insert_speakers(connection)

    create_emotion_table(connection)
    insert_emotions(connection)

    connection.commit()
    connection.close()
