# coding: utf-8
import os;
import sys;
import re;
from io import open;

from normdatei.text import clean_text, clean_name, fingerprint;
from normdatei.parties import search_party_names;
from groupA.emotionparser import parse_emotion

def file_metadata(filename):
    fname = os.path.basename(filename)
    return int(fname[:2]), int(fname[2:5])

CHAIRS = ['Vizepräsidentin', 'Vizepräsident', 'Präsident', 'Präsidentin', 'Alterspräsident', 'Alterspräsidentin'];

SPEAKER_STOPWORDS = ['ich zitiere', 'zitieren', 'Zitat', 'zitiert',
                     'ich rufe den', 'ich rufe die',
                     'wir kommen zur Frage', 'kommen wir zu Frage', 'bei Frage',
                     'fordert', 'fordern', u'Ich möchte',
                     'Darin steht', ' Aspekte ', ' Punkte ', 'Berichtszeitraum']

BEGIN_MARK = re.compile('Beginn:? [X\d]{1,2}.\d{1,2} Uhr')
END_MARK = re.compile('(\(Schluss:.\d{1,2}.\d{1,2}.Uhr\).*|Schluss der Sitzung)')

# speaker types
PARTY_MEMBER = re.compile('\s*(.{5,140}\(.*\)):\s*$')
PRESIDENT = re.compile('\s*((Alterspräsident(?:in)?|Vizepräsident(?:in)?|Pr\xe4sident(?:in)?|Präsident(?:in)?).{5,140}):\s*$')
STAATSSEKR = re.compile('\s*(.{5,140}, Parl\. Staatssekretär.*):\s*$')
STAATSMINISTER = re.compile('\s*(.{5,140}, Staatsminister.*):\s*$')
MINISTER = re.compile('\s*(.{5,140}, Bundesminister.*):\s*$')
WEHRBEAUFTRAGTER = re.compile('\s*(.{5,140}, Wehrbeauftragter.*):\s*$')
BUNDESKANZLER = re.compile('\s*(.{5,140}, Bundeskanzler.*):\s*$')
BEAUFTRAGT = re.compile('\s*(.{5,140}, Beauftragter? der Bundes.*):\s*$')

TOP_MARK = re.compile('.*(?: rufe.*der Tagesordnung|Tagesordnungspunkt|Zusatzpunkt)(.*)')
POI_MARK = re.compile('\((.*)\)\s*$', re.M)
POI_START_MARK = re.compile('^\(([^)\n]+)$')
POI_END_MARK = re.compile('^([^(\n]+)\)\s*$')

# some protocolls contain strange unicode-chars (dots?). Remove them as they break the regexes!
def clean_strange_unicode_char(line):
    return line.replace("\u200e","")

class SpeechParser(object):
    def __init__(self, lines):
        self.lines = lines
        self.was_chair = True

    def parse_pois(self, group):
        for poi in group.split(' - '):
            text = poi
            speaker_name = None
            sinfo = poi.split(': ', 1)
            if len(sinfo) > 1:
                speaker_name = sinfo[0]
                text = sinfo[1]
            yield (speaker_name, text)

    def __iter__(self):
        self.in_session = False
        self.chair = False
        self.text = []
        self.speaker = None

        def emit():
            data = {
                'speaker': self.speaker,
                'type': 'chair' if self.chair else 'speech',
                'text': "\n\n".join(self.text).strip()
            }
            self.was_chair = self.chair
            self.text = []
            return data

        def emit_poi(speaker, text):
            self.was_chair = False
            return {
                'speaker': speaker,
                'type': 'poi',
                'text': text
            }

        poi_start = None
        for line in self.lines:
            line = clean_strange_unicode_char(line)
            rline = line.strip()

            if not self.in_session and BEGIN_MARK.match(line):
                self.in_session = True
                continue
            elif not self.in_session:
                continue

            if END_MARK.match(rline):
                return

            if not len(rline):
                continue

            is_top = False
            if TOP_MARK.match(line):
                is_top = True

            has_stopword = False
            for sw in SPEAKER_STOPWORDS:
                if sw.lower() in line.lower():
                    has_stopword = True

            speaker_match = (PRESIDENT.match(line) or
                             PARTY_MEMBER.match(line) or
                             STAATSSEKR.match(line) or
                             STAATSMINISTER.match(line) or
                             WEHRBEAUFTRAGTER.match(line) or
                             BUNDESKANZLER.match(line) or
                             BEAUFTRAGT.match(line) or
                             MINISTER.match(line))
            if speaker_match is not None \
                    and not is_top \
                    and not has_stopword:
                if self.speaker is not None and self.text:
                    yield emit()
                role = line.strip().split(' ')[0]
                self.speaker = speaker_match.group(1)
                self.chair = role in CHAIRS
                continue

            poi_match = POI_MARK.match(rline)
            if poi_match is not None:
                # if not poi_match.group(1).lower().strip().startswith('siehe'):
                yield emit()
                for speaker, text in self.parse_pois(poi_match.group(1)):
                    yield emit_poi(speaker, text)
                continue

            poi_start_match = POI_START_MARK.match(rline)
            if poi_start_match is not None:
                poi_start = poi_start_match.group(1)
                continue

            poi_end_match = POI_END_MARK.match(rline)
            if poi_end_match is not None and poi_start is not None:
                yield emit()
                poi = poi_start + poi_end_match.group(1)
                for speaker, text in self.parse_pois(poi):
                    yield emit_poi(speaker, text)
                continue

            self.text.append(rline)
        yield emit()

 
def get_speeker_list(txt_file):
    if not os.path.exists(txt_file):
        print("file not found");
        sys.exit(0);
        
    wp, session = file_metadata(txt_file)    
    text = "";
    try:
        with open(txt_file) as fh:
            content = fh.read()
            text = clean_text(content)
    except UnicodeDecodeError:
        #print("Reloading in other encoding (windows-1252)")
        with open(txt_file, encoding="windows-1252") as fh:
            content = fh.read()
            text = clean_text(content)

    base_data = {
            'filename': txt_file,
            'sitzung': session,
            'wahlperiode': wp
        }

    parser = SpeechParser(text.split('\n'))

    seq = 0;
    entries = []
    for contrib in parser:
        contrib.update(base_data)
        contrib['sequence'] = seq
        contrib['speaker_cleaned'] = clean_name(contrib['speaker'])
        contrib['speaker_fp'] = fingerprint(contrib['speaker_cleaned'])
        contrib['speaker_party'] = get_party(contrib)
        contrib['top_id'] = 0
        contrib['emotion'] = parse_emotion(contrib['type'], contrib['speaker_fp'], contrib['text'])
        seq += 1
        entries.append(contrib)
    
    return entries


def get_party(contrib):
    parties = None

    if contrib['type'] == 'poi':
        parties = search_party_names(contrib['speaker'])
        if parties is None:
            parties = search_party_names(contrib['text'])
    else:
        parties = search_party_names(contrib['speaker'])

    return parties
