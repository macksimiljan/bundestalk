# coding: utf-8
from normdatei.text import clean_text, clean_name, fingerprint;


def findFirstNonemptyTop(li):
    topidx = 0
    for top in li:
        if top["speakers"]:
            return topidx
        topidx +=1
    return -1


def merge_tops_speaker(topsList, speakerList):

    # sometimes a top has no speakers, but we need to find the first speaker. So get the first top with a speaker
    firsttop = findFirstNonemptyTop(topsList)
    if firsttop == -1:
        return speakerList

    wait_for = fingerprint(clean_name(topsList[firsttop]["speakers"][0]))
    tops_index = 0
    speaker_index = 0

    speaks = iter(speakerList)
    current = speaks.__next__()

    try:
        # iterate over all topics and planned speakers
        for top in topsList:
            for planned in top["speakers"]:
                # consume everything up to the planned speaker
                planned = fingerprint(clean_name(planned))
                while not current["speaker_fp"] == planned or current["type"] != "speech":
                    current["top_id"] = topsList[tops_index]["id"]
                    current = speaks.__next__()
                
                # consume all speeches from the planned speaker
                while current["speaker_fp"] == planned or current["type"] != "speech":
                    current["top_id"] = topsList[tops_index]["id"]
                    current = speaks.__next__()

                speaker_index += 1
            tops_index += 1
    except StopIteration:
        pass
        
    return speakerList