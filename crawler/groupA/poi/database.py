from sqlite3 import connect
from sqlite3 import Error


# http://www.sqlitetutorial.net/sqlite-python/sqlite-python-select/
class Database:

    def __init__(self, db_file):
        self.connection = self.create_connection(db_file)

    def create_connection(self, db_file):
        """ create a database connection to the SQLite database
                specified by the db_file
            :param db_file: database file
            :return: Connection object or None
            """
        try:
            conn = connect(db_file)
            return conn
        except Error as e:
            print(e)

        return None

    def select_poi_where_speaker_is_null(self):
        cursor = self.connection.cursor()
        cursor.execute(" SELECT text"
                       " FROM speak"
                       " WHERE type='poi'"
                       " AND speaker IS NULL;")
        rows = cursor.fetchall()
        cursor.close()
        return rows

    def select_all_politician_names(self):
        cursor = self.connection.cursor()
        cursor.execute(" SELECT name"
                       " FROM speakers;")

        rows = cursor.fetchall()
        cursor.close()
        return rows
