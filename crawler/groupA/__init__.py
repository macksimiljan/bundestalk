# coding: utf-8
import os;
import requests;
import sqlite3;
import sys;
from groupA.SpeakerScraper import get_speeker_list;
from groupA.MetadataScraper import parse_metadata;
from groupA.TopsSpeakerMerger import merge_tops_speaker;
#import pprint;

def createDB(db_file):
    conn = sqlite3.connect(db_file)
    
    conn.execute('''CREATE TABLE IF NOT EXISTS tops (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    wp INTEGER,
                    s INTEGER,
                    sequence INTEGER,
                    topic TEXT,
                    UNIQUE (wp, s, sequence)
                    )''')
                    
    conn.execute('''CREATE TABLE IF NOT EXISTS speak (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    wp INTEGER,
                    s INTEGER,
                    sequence INTEGER,
                    top_id INTEGER,
                    type VARCHAR(20),
                    speaker VARCHAR(200),
                    speaker_cleaned VARCHAR(200),
                    speaker_fp VARCHAR(200),
                    speaker_party VARCHAR(200),
                    text TEXT,
                    emotion VARCHAR(25),
                    UNIQUE (wp, s, sequence)
                    )''')
                    
    conn.execute('''CREATE TABLE IF NOT EXISTS protocol (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    wp INTEGER,
                    s INTEGER,
                    url TEXT NULL,
                    date DATE NULL,
                    UNIQUE (wp, s)
                    )''')
                    
               
            
def scape(txt_file, DATABASE_URI, PROTOCOL_HTML_DIR):

    # these Protocolls have invalid syntax (no tops, complete gibberish etc.)
    broken = {"18119.txt","18223.txt","17053.txt","18004.txt","17169.txt","17148.txt"}

    # skip broken
    if os.path.basename(txt_file) in broken:
        return

    #pp = pprint.PrettyPrinter(indent=4);
    speakerList = get_speeker_list(txt_file);
    ##pp.pprint(speakerList);
    try:
        protocoll_metadata = parse_metadata(txt_file);
    except ValueError as err:
        print("Metadataparser failed for:",txt_file,err)
        sys.exit(1)

    ##pp.pprint(topsList)
    speakerList = merge_tops_speaker(protocoll_metadata["tops"], speakerList);
    #pp.pprint(speakerList)

    
    #Insert Data in Database
    conn = sqlite3.connect(DATABASE_URI)
    c = conn.cursor()
    DbTopsDic = {0: 0};
    for top in protocoll_metadata["tops"]:
        c.execute('''INSERT INTO tops (wp, s, sequence, topic) 
                    values (?,?,?,?)''', (protocoll_metadata["wp"], protocoll_metadata["session"], top["id"], top["topic"]));
        DbTopsDic[top["id"]] = c.lastrowid;

    c.execute("INSERT INTO protocol (wp,s,url,date) VALUES (?,?,?,?)",(protocoll_metadata["wp"],protocoll_metadata["session"],protocoll_metadata["url"],protocoll_metadata["date"]))

    for speaker in speakerList:
        c.execute('''INSERT OR IGNORE INTO speak(wp, s, top_id, sequence, type, speaker, speaker_cleaned, speaker_fp, speaker_party, text, emotion) 
                    values (?,?,?,?,?,?,?,?,?,?,?)''', (speaker["wahlperiode"], speaker["sitzung"], DbTopsDic[speaker["top_id"]], speaker["sequence"], speaker["type"], speaker["speaker"], speaker["speaker_cleaned"], speaker["speaker_fp"], speaker["speaker_party"], speaker["text"], speaker["emotion"]));
    conn.commit()
    
    #create protocoll html
    protocol_html_file = os.path.join(PROTOCOL_HTML_DIR, str(protocoll_metadata["wp"]) + '_' + str(protocoll_metadata["session"]) + '.html')
    createHtmlProtocol(protocol_html_file, conn, protocoll_metadata["wp"], protocoll_metadata["session"])
    print("Create {}".format(protocol_html_file))
    c.close()
        
def createHtmlProtocol(path, connection, wp, s):
    if os.path.exists(path):
        os.remove(path)
        
    file = open(path, 'w')

    sql = '''
        SELECT  id,
                IFNULL(speaker, '') as speaker,
                text,
		IFNULL(speaker_party, '') AS party
		FROM speak
		WHERE wp = {} and s = {}
		ORDER BY id ASC'''
    
    cursor = connection.cursor()
    cursor.execute(sql.format(wp, s))
    
    previousName = "";
    
    for (id, speaker, text, party) in cursor:
        file.write('<div class="row speech" speech-id="' + str(id)  + '">')
        file.write('<div class="col-md-12">')
        
        if speaker != '' and previousName != speaker:
            file.write('<div class="speaker">')
            file.write('<div class ="person">' + speaker + '</div>')
            
            if party != "":
                for partyName in party.split(":") :
                    file.write('<div class ="party"><img src="media/parteien/' + partyName + '.png"></div>')
        
            file.write('</div>')
            
        if speaker == "":
            file.write('<span class="text emotion">' + text + '</span>')
            if party != "":
                for partyName in party.split(":") :
                    file.write('<img class="partyP" src="media/parteien/' + partyName + '.png">')
        else:
            file.write('<div class ="text">' + text + '</div>')
        
        file.write('</div>')
        file.write('</div>')
        
        if speaker != "":
            previousName = speaker
    cursor.close()
    file.close()