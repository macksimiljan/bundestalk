#!/usr/env/python


import requests
import os
from lxml import html
from urllibX.parse import urljoin

INDEX_URL = 'https://www.bundestag.de/plenarprotokolle'
ARCHIVE_URL = 'http://webarchiv.bundestag.de/archive/2013/0927/dokumente/protokolle/plenarprotokolle/plenarprotokolle/17%03.d.txt'


def download(urls,TXT_DIR):
    try:
        os.makedirs(TXT_DIR)
    except:
        pass
    currentUrl = 0
    numberOfUrls = len(urls)
    for url in urls:
        currentUrl += 1
        txt_file = os.path.join(TXT_DIR, os.path.basename(url))
        txt_file = txt_file.replace('-data', '')
        if os.path.exists(txt_file):
            print("File {} already present. Skipping...".format(txt_file))
            continue

        if "bv.txt" in txt_file:
            print("Ignoring Bundesversammlungs-protokoll. The parser doesn't like them")
            continue

        r = requests.get(url)
        if r.status_code < 300:
            with open(txt_file, 'wb') as fh:
                try:
                    ans = r.content.decode("UTF-8").encode("UTF-8")
                except UnicodeDecodeError:
                    ans = r.content.decode("ISO-8859-1").encode("UTF-8")
                fh.write(ans)
                print("Downloaded {}/{}: ".format(currentUrl,numberOfUrls),url, txt_file)


def get_urls():
    urls = set()
    res = requests.get(INDEX_URL)
    doc = html.fromstring(res.content)
    for a in doc.findall('.//a'):
        url = urljoin(INDEX_URL, a.get('href'))
        if url.endswith('.txt'):
            urls.add(url)

    for i in range(30, 260):
        url = ARCHIVE_URL % i
        urls.add(url)

    new_urls = _get_new_urls()
    urls = urls.union(new_urls)
    offset = 20
    while len(new_urls) == 20:
        new_urls = _get_new_urls(offset)
        urls = urls.union(new_urls)
        offset += 20
    return urls


    
def _get_new_urls(offset=0):
    base_url = "https://www.bundestag.de"
    url = "https://www.bundestag.de/ajax/filterlist/de/dokumente/protokolle/plenarprotokolle/plenarprotokolle/-/455046/"
    params = {"limit": 20,
              "noFilterSet": "true",
              "offset": offset
              }
    res = requests.get(url, params=params)
    doc = html.fromstring(res.content)
    return {urljoin(base_url, a.get('href')) for a in doc.findall('.//a')}

