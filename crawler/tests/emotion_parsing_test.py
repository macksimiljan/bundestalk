import unittest

from groupA.SpeakerScraper import SpeechParser

class EmotionParsingtest(unittest.TestCase):

    def test_one_line_emotion(self):
        text = """
Beginn: 12:30 Uhr

Hans Müller (Partei):
Wir werden jetzt schon – das hat Anton Hofreiter gesagt – vor dem WTO-Staat-zu-Staat-Schiedsverfahren genau deshalb verurteilt, weil dieses Vorsorgeprinzip nicht verankert ist. Dasselbe gilt für das Right to regulate, dasselbe gilt für die Absicherung des „hohen Schutzniveaus“. All das steht in unserem Antrag. 
(Beifall beim BÜNDNIS 90/DIE GRÜNEN)
Von der CDU/CSU habe ich keinen einzigen Satz zum Inhalt dieses Antrags gehört, keinen einzigen Satz zur Debatte, stattdessen nur Verfahrenskritik oder Beschimpfungen der Zivilgesellschaft. 
            """
        parser = SpeechParser(text.split('\n'))
        lines = list(parser)
        emotion = {'type': 'poi', 'text': 'Beifall beim BÜNDNIS 90/DIE GRÜNEN', 'speaker': None}
        self.assertEqual(lines[1], emotion)

    def test_multi_line_emotion(self):
        text = """
Beginn: 12:30 Uhr

Katharina Dröge (BÜNDNIS 90/DIE GRÜNEN): 
  Stimmt, das ist dein Job.
(Heiterkeit beim BÜNDNIS 90/
DIE GRÜNEN)
  Deswegen komme ich jetzt zum Ende meiner Rede. 
        """

        parser = SpeechParser(text.split('\n'))
        lines = list(parser)
        emotion = {'type': 'poi', 'text': 'Heiterkeit beim BÜNDNIS 90/DIE GRÜNEN', 'speaker': None}
        self.assertEqual(lines[1], emotion)