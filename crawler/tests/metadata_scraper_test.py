# -!- coding:utf-8 -!-

import unittest
from groupA.MetadataScraper import TopParser
from normdatei.text import clean_text


class MetadataScraperTest(unittest.TestCase):

    def basic_parser_test(self):
        text = """
Deutscher Bundestag
Stenografischer Bericht
238. Sitzung
Berlin, Freitag, den 2. Juni 2017
Inhalt:

Tagesordnungspunkt 39:
Unterrichtung durch die Bundesregierung: Legislaturbericht Digitale Agenda 2014 bis 2017
Drucksache 18/12130	
 24309 B
Brigitte Zypries, Bundesministerin 
BMWi	
 24309 D
Dr. Petra Sitte (DIE LINKE)	
 24311 A
Dr. Thomas de Maizière, Bundesminister BMI	
 24312 B
Dieter Janecek (BÜNDNIS 90/
DIE GRÜNEN)	
 24313 C
Zusatztagesordnungspunkt 8:
Eidesleistung der Bundesministerin für Familie, Senioren, Frauen und Jugend	
 24326 D
Dr. Katarina Barley, Bundesministerin 
BMFSFJ	
 24327 A  
Tagesordnungspunkt 40:
Antrag der Bundesregierung: Fortsetzung der Beteiligung bewaffneter deutscher Streitkräfte an EUNAVFOR MED Operation SOPHIA
Drucksache 18/12491	
 24327 B
Sigmar Gabriel, Bundesminister AA	
 24327 C
Jürgen Hardt (CDU/CSU)	
 24331 A 

238. Sitzung
Berlin, Freitag, den 2. Juni 2017
Beginn: 9.00 Uhr 
        """
        prepocessed_text = clean_text(text).split('\n')
        result = TopParser(prepocessed_text).parse()
        date = result['date']
        topics = result['tops']

        self.assertEqual(3, len(topics))
        self.assertEqual(4, len(topics[0]['speakers']))
        self.assertEqual('Brigitte Zypries, Bundesministerin BMWi', topics[0]['speakers'][0])
        self.assertEqual('Dr. Thomas de Maizière, Bundesminister BMI', topics[0]['speakers'][2])
        self.assertEqual('Dieter Janecek (BÜNDNIS 90/DIE GRÜNEN)', topics[0]['speakers'][-1])
        self.assertEqual('Unterrichtung durch die Bundesregierung: Legislaturbericht Digitale Agenda 2014 bis 2017\nDrucksache 18/12130\t\n', topics[0]['topic'])

    def subdivided_top_test(self):
        text = """
Deutscher Bundestag
Stenografischer Bericht
238. Sitzung
Berlin, Freitag, den 2. Juni 2017
Inhalt:
        
Tagesordnungspunkt 44:
a)	Unterrichtung durch die Bundesregierung: Aktionsplan der Bundesregierung zur Umsetzung von Resolution 1325 zu Frauen, Frieden, Sicherheit des Sicherheitsrats der Vereinten Nationen für den Zeitraum 2017 bis 2020
Drucksache 18/10853	
 24357 D
b)	Unterrichtung durch die Bundesregierung: Umsetzungsbericht zum Aktionsplan der Bundesregierung zur Umsetzung von Resolution 1325 des Sicherheitsrats der Vereinten Nationen für den Zeitraum 2013 bis 2016
Drucksache 18/10852	
 24357 D
Dr. Maria Böhmer, Staatsministerin AA	
 24358 A
Kathrin Vogler (DIE LINKE)	
 24359 C
Gabriela Heinrich (SPD)	
 24360 D
Dr. Franziska Brantner (BÜNDNIS 90/
DIE GRÜNEN)	
 24361 C 

238. Sitzung
Berlin, Freitag, den 2. Juni 2017
Beginn: 9.00 Uhr     
        """

        prepocessed_text = clean_text(text).split('\n')
        result = TopParser(prepocessed_text).parse()
        topics = result['tops']

        self.assertEqual(1, len(topics))
