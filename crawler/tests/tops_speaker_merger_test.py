import unittest
from groupA.TopsSpeakerMerger import merge_tops_speaker


class TopsSpeakerMergerTest(unittest.TestCase):

    def basic_test(self):
        # tops and speeches can be "joined" by speakers
        tops_list = [
            {'id': 1, 'topic': 'TOP 1', 'speakers': ['Präsident John Doe', 'Maria Mustermann'],
             'filename': 'file.txt', 'sitzung': 1, 'wahlperiode': 1},
            {'id': 2, 'topic': 'TOP 2', 'speakers': ['Dr. John Smith'],
             'filename': 'file.txt', 'sitzung': 1, 'wahlperiode': 1},
            {'id': 3, 'topic': 'TOP 3', 'speakers': ['Maria Mustermann'],
             'filename': 'file.txt', 'sitzung': 1, 'wahlperiode': 1}
        ]
        speech_list = [
            {'sequence': 0, 'speaker': 'John Doe', 'speaker_cleaned': 'John Doe', 'speaker_fp': 'john-doe',
             'speaker_party': 'spd', 'text': 'I have something to say.', 'top_id': 0, 'type': 'speech',
             'filename': 'file.txt', 'sitzung': 1, 'wahlperiode': 1},
            {'sequence': 0, 'speaker': 'Someone', 'speaker_cleaned': 'Someone', 'speaker_fp': 'someone',
             'speaker_party': 'spd', 'text': 'Zwischenruf!', 'top_id': 0, 'type': 'poi',
             'filename': 'file.txt', 'sitzung': 1, 'wahlperiode': 1},
            {'sequence': 2, 'speaker': 'Maria Mustermann', 'speaker_cleaned': 'Maria Mustermann', 'speaker_fp': 'maria-mustermann',
             'speaker_party': 'linke', 'text': 'I have something to say as well.', 'top_id': 0, 'type': 'speech',
             'filename': 'file.txt', 'sitzung': 1, 'wahlperiode': 1},
            {'sequence': 3, 'speaker': 'Dr. John Smith', 'speaker_cleaned': 'Dr. Johne Smith', 'speaker_fp': 'john-smith',
             'speaker_party': 'spd', 'text': 'I say nothing.', 'top_id': 0, 'type': 'speech',
             'filename': 'file.txt', 'sitzung': 1, 'wahlperiode': 1},
            {'sequence': 4, 'speaker': 'Maria Mustermann', 'speaker_cleaned': 'Maria Mustermann', 'speaker_fp': 'maria-mustermann',
             'speaker_party': 'linke', 'text': 'Again: I have something to say.', 'top_id': 0, 'type': 'speech',
             'filename': 'file.txt', 'sitzung': 1, 'wahlperiode': 1},

        ]
        speech_list = merge_tops_speaker(tops_list, speech_list)

        self.assertEqual(5, len(speech_list))
        self.assertEqual('John Doe', speech_list[0]['speaker'])
        self.assertEqual('I have something to say.', speech_list[0]['text'])

        self.assertEqual(1, speech_list[0]['top_id'])
        self.assertEqual(1, speech_list[1]['top_id'])
        self.assertEqual(1, speech_list[2]['top_id'])
        self.assertEqual(2, speech_list[3]['top_id'])
        self.assertEqual(3, speech_list[4]['top_id'])
