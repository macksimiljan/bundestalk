import unittest

import groupA.dateparser as DateParser


class DateParserTest(unittest.TestCase):

    def basic_parser_test(self):
        text = '2. Juni 2017'
        date = DateParser.parse(text)
        self.assertEqual('2017-06-02 00:00:00', str(date))