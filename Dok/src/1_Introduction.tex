\section{Einleitung}\label{Introduction}
Eine reflektierte Partizipation am politischen System erfordert Transparenz von politischen Entscheidungsprozessen. In der Bundesrepublik Deutschland werden Entscheidungen im Bundestag getroffen und zusammen mit den dazugehörigen Debatten in Plenarprotokollen festgehalten. Interessierte Bürger und Bürgerinnen besitzen das Informationsbedürfnis, den politischen Entscheidungsprozess aus einer bestimmten Perspektive nachzuvollziehen. Eine solche Perspektive kann beispielsweise thematisch sein, wie das Bedürfnis, die Diskussion zur Energiewende zu rekapitulieren, die sich auf mehrere Sitzungen und verschiedene Politiker und Politikerinnen verteilt; oder auch personen-bezogen, z.B. das Interesse an den Redebeiträge eines Politikers oder einer Politikerin aus demselben Wahlkreis. Ein Informationssystem, das dazu beitragen soll, den politischen Entscheidungsprozess nachvollziehbar zu machen, hat als grundlegende Daten die Plenarprotokolle des Bundestags. Diese sind so aufbereitet, dass sie durchsucht werden können.

% --------------------------
\subsection*{Informationssysteme zu Bundestagsreden}
% http://dipbt.bundestag.de/dip21.web/bt
Es existieren zwei Systeme, die es ermöglichen, Bundestagsreden zu durchsuchen. Eines ist das Dokumentations- und Informationssystem für Parlamentarische Vorgänge (DIP)\footnote{\url{http://dipbt.bundestag.de/dip21.web/bt}}, das umfangreiches Material zum parlamentarischen Geschehen zur Verfügung stellt. Unter anderem wird eine Suche angeboten\footnote{\url{http://dipbt.bundestag.de/dip21.web/searchDocuments/plpr_search_text.do?text=true}}, um gezielt Reden auszuwählen, die von einem oder einer Abgeordneten sind und/ oder einen bestimmten Suchbegriff enthalten. Zudem kann nach Zeitraum und Nummer des Protokolls gefiltert werden. Die Ergebnisliste besteht aus Links zu den kompletten Protokollen.

% OffenesParlament
Ein weiteres Informationssystem ist OffenesParlament\footnote{\url{https://offenesparlament.de/}}, das ein Projekt der Open Knowledge Foundation Deutschlad ist. Datengrundlage sind die Plenarprotokolle des Bundestags der 18. Wahlperiode (Oktober 2013 bis Juni 2017). Die Daten zu Politikerinnen und Politikern wurden durch weitere Quellen angereichert. Die Suche erlaubt es, einen Suchbegriff, ein Thema, einen Redner oder eine Rednerin sowie ein Jahr anzugeben. Ergebnis der Suche ist ebenfalls eine Liste von Links zu den Plenarprotokollen. Ein weiterer Schwerpunkt von OffenesParlament ist die statistische Analyse der Daten -- wie die Frage nach den häufigsten Themen oder nach dem Politiker oder der Politikerin mit den meisten Redebeiträgen.

Das vorliegende Projekt (mit dem Arbeitstitel \textit{Bundestalk}) implementiert ein weiteres Informationssystem zum parlamentarischen Diskurs. Es zeichnet sich dadurch aus, dass mehr Wahlperioden als bei OffenesParlament zugrundegelegt wurden, dass die Suche verbessert und erweitert wurde und dass die Ergebnisliste nicht Protokolllinks sondern Redebeiträge enthält. Letzteres führt dazu, dass die entwickelte Suchmaschine auf einem anderen Dokumenbegriff als die beiden anderen aufbaut. Zudem wurden Emotionen wie Beifall oder Applaus auf Redebeiträge geparst und der Suche hinzugefügt.


% --------------------------
\begin{table}
	\caption{User Stories: \texttt{Als $<$\emph{Rolle}$>$ möchte ich $<$\emph{Ziel}$>$, um $<$\emph{Nutzen}$>$.}}
	\label{tab:user_stories}
	\renewcommand{\arraystretch}{2}
	\begin{tabular}{clp{5cm}p{5cm}}
		\toprule
		Id   & Rolle        & Ziel        &  Nutzen   \\
		\midrule
		1 & Politikinteressierte & Redebeiträge zu einem bestimmten Thema suchen  & Informationen zur politischen Debatte zu erhalten \\
		2 & Politikinteressierte & Redebeiträge eines bestimmten Politikers suchen & Hilfe bei der Wahlentscheidung (für meinen Wahlkreis) zu erhalten \\
		3 & Politikinteressierte & Aussagen eines Politikers zu einem Thema über einen großen Zeitraum hinweg einsehen & Veränderung der Meinung eines Politikers zu einem Thema zu bemerken\\
		4 & Politikinteressierte & gleiche Emotionen zweier Parteien zu einem Thema finden & Übereinstimmung von Parteien zu beurteilen\\
		5 & Lehrer & rhetorisch gute Reden [= viele Emotionen] finden & den Aufbau/ Vortrag einer Rede zu besprechen\\
		6 & Journalist & Zitate finden & Aussagen eines Politikers im Kontext zu lesen\\
		7 & Journalist & Veränderung von Meinungen zu einem Thema finden & die Bevölkerung zu informieren\\
		8 & Journalist & Abstimmungsergebnisse zu einem bestimmten TOP suchen & die Bevölkerung zu informieren\\
		9 & Journalist & besondere Events im Bundestag finden (z.B. Abgeordnete verlässt den Saal) & Interview vorbereiten\\
		10 & Zuarbeiter & Veränderung von Meinungen zu einem Thema finden & politischen Gegner anzugreifen\\
		11 & Zuarbeiter & die wichtigesten Wörter im Kontext eines bestimmten Themas finden & Hintergrund zu einem Thema zu erhalten\\
		12 & Zuarbeiter & wer auf wen in ihrer Rede Bezug genommen hat & Zitierungsgraph zu erstellen\\
		\bottomrule
	\end{tabular}
	\renewcommand{\arraystretch}{1}
\end{table}


\subsection*{User Stories}
Es wurden vier User-Rollen und zwölf User Stories für Bundestalk identifiziert. Die Liste der User Stories finden sich in Tab.~\ref{tab:user_stories}. Die Rolle der Politikinteressierten ist die mit dem geringsten Expertenwissen. Ein grundlegendes Ziel ist das Suchen von Redebeiträgen zu einem bestimmten Thema, um Informationen zu der entsprechenden parlamentarischen Debatte zu erhalten. Ein weiteres Ziel ist, die Redebeiträge einer bestimmten Politikerin oder eines bestimmten Politikers zu suchen, um beispielsweise Hilfe bei der Wahlentscheidung für oder gegen Kandidierende aus dem Wahlkreis zu bekommen.

Eine weitere User-Rolle sind Lehrende. Ein mögliches Ziel ist das Finden von rhetorisch guten Reden.\footnote{Eine rhetorisch gute Rede kann beispielsweise durch das Auftreten vieler verschiedener Emotionen von Widerspruch über Zwischenfragen bis hin zu Beifall definiert werden.} Die Rolle des Journalisten bzw. der Journalistin verfolgt Ziele, die ein unterschiedliches Analyseniveau der Protokolle erfordern. Das Suchen nach Zitaten ist eine einfache Volltextsuche. Das Suchen nach Abstimmungsergebnissen hingegen erfordert, dass Abstimmungsergebnisse als solche beim Parsen erkannt und entsprechend aufbereitet werden.

Die vierte Rolle umfasst Zuarbeitende von Politikerinnen und Politikern, z.B. zum Schreiben von Reden oder zum Erstellen von Spickzetteln von Verhandlungen. Die erstellten User Stories dieser Rolle erfordern die stärkste Aufarbeitung der Protkolle und gegebenfalls die Erweiterung des Dokumentbegriffs. Um einen Zitierungsgraphen zu erstellen, ist Named Entity Recognition notwendig, sodass Verweise zu Personen, Firmen und Parteien in Redebeiträgen gefunden werden.


% --------------------------
\subsection*{Dokumentbegriff}

Ein Dokument im Sinne von Bundestalk ist ein Ausschnitt aus einem Redebeitrag im Bundestag. Ein Redebeitrag beginnt i.d.R. mit der Ankündigung der Rednerin oder des Redners $R$ von Partei $P$ durch den Bundestags(vize)präsidenten oder die Bundestags(vize)präsidentin. Hierauf erfolgt Applaus von $P$. Die folgende Rede von $R$ kann unterschiedliche Emotionen wie \glqq Beifall bei der SPD\grqq{} oder \glqq Zuruf von der AfD\grqq{} enthalten. In diesem Fall entspricht ein Redebeitrag inklusive Ankündigung und Emotionen einem Dokument. Abweichungen hiervon kann es beispielsweise bei Zwischenfragen geben. In solchen Fällen findet sich innerhalb eines Redebeitrags von $R$ ein weiterer Redebeitrag einer anderen Politikerin oder eines anderen Politikers $R'$. Das Beispiel in Abb.~\ref{fig:bsp_dokument_begriff} verdeutlicht dies. Das erste Dokument ist der erste Teil der Rede von Bartels. Das zweite Dokument ist die Zwischenfrage von Brandl einschließlich der Ankündigung durch den Bundestagspräsidenten. Das dritte Dokument ist der darauf folgende Teil der Rede von Bartels.

\begin{figure}
\centering
\includegraphics[scale=0.65]{bartels_eps}
\caption{Ausschnitt aus dem Protokoll der 240. Sitzung, 17. Wahlperiode des Deutschen Bundestags.}
\label{fig:bsp_dokument_begriff}
\end{figure}


% --------------------------
\subsection*{Statistik zu den Daten}

Insgesamt liegen etwa $53 \cdot 10^3$ Dokumente vor. Die Durchschnittslänge beträgt $3677,6$ Zeichen. Die Verteilung der Dokumentlänge ist in Abb.~\ref{fig:document_length} dargestellt. Etwa zwei Drittel aller Dokumente haben eine Länge zwischen $10^3$ und $10^4$ Zeichen. Die Anzahl der Dokumente pro Politiker/in ist in Abb.\ref{fig:docs_per_politicians} zu sehen. Im Durchschnitt sind einem Politiker/ einer Politikerin $48,3$ Dokumente zugeordnet. Die Top-3 sind Volker Beck mit 587 Dokumenten, Hans-Christian Strobele mit 496 Dokumenten und Katherina Reiche mit 422 Dokumente. Insgesamt wurden zehn Emotionen manuell identifiziert, wobei \textit{Beifall} mit $91,4\%$ Auftretenswahrscheinlichkeit die häufigste Emotion ist. Wie oft Emotionen in einem Dokument vorkommen, zeigt Abb.~\ref{fig:sum_emotions_pro_doc}. In knapp $22 \cdot 10^3$ Dokumenten kommt keine Emotion vor, in etwa $3 \cdot 10^3$ eine Emotion und zwei oder mehr Emotionen in etwa $28,3 \cdot 10^3$ Dokumenten. Abb.~\ref{fig:docs_per_protocol} schließlich illustriert, wie viele Dokumente ein Plenarprotokoll enthält. Im Mittel setzt sich ein Protokoll aus $113,2$ Dokumenten zusammen (Standardabweichung von $56,7$ Dokumenten). Die drei Protokolle mit mehr als 300 Dokumenten enthalten, wie erwartet aufgrund der Definition des Dokumentbegriffs, einen hohen Anteil an Zwischenfragen.

\begin{figure*}
    \centering
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{document_length_frequency_eps}
        \caption{Häufigkeit von Dokumentlänge.}
        \label{fig:document_length}
    \end{subfigure}%
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{frequency_politicians_eps}
        \caption{Häufigkeit von Redebeiträge von Politikerinnen und Politikern.}
        \label{fig:docs_per_politicians}
    \end{subfigure}
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{sum_emotions_pro_doc_eps}
        \caption{Häufigkeit von Emotionen in Dokumenten.}
        \label{fig:sum_emotions_pro_doc}
    \end{subfigure}%
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{docs_per_protocol_eps}
        \caption{Häufigkeit von Dokumenten pro Protokoll.}
        \label{fig:docs_per_protocol}
    \end{subfigure}
    \caption{Statistik der Plenarprotokolle von Wahlperiode 17 bis 19.}
\end{figure*}


