\newpage
\section{Textbeschaffung}\label{Acquisition}
\subsection{Crawling}

Die Redeprotokolle aller Sitzungen des Bundestags sind auf der Webseite des Bundestages\footnote{\url{https://www.bundestag.de/protokolle}} öffentlich zugänglich.

Die Protokolle der 1. bis 13. Wahlperiode wurden durch Einscannen und OCR nachträglich digitalisiert, ab der 14. Wahlperiode stehen alle Protokolle direkt als PDF zur Verfügung.
Die PDF-Versionen aller Protokolle sind bequem über URLs nach dem Schema \begin{center}
{\footnotesize \url{http://dip21.bundestag.de/dip21/btp/<wahlperiode>/<wahlperiode><3-stellige-Sitzungsnr>.pdf}}
\end{center} abrufbar. Somit wäre es problemlos möglich, automatisiert alle PDF-Protokolle herunterzuladen. Um diese gezielt durchsuchen zu können, wäre es allerdings zuerst nötig die PDF-Dateien in Plaintext zu konvertieren. Allerdings werden für das in Kapitel 1 beschriebene Dokumentenmodell noch mehr Informationen als nur der reine Text benötigt. Es braucht zusätzlich noch Informationen zu Beginn und Ende sowie dem Redner der einzelnen Redebeiträge.
Leider sind diese aufgrund des Aufbaus der PDF-Dateien nur schwer extrahierbar, zumal sich der exakte Aufbau der Protokolle über die Zeit hinweg verändert.

Glücklicherweise stehen die Protokolle auch als txt-Version zur Verfügung. Der Aufbau der txt-Dateien ist erheblich simpler und dadurch einfacher automatisiert verarbeitbar. Problematisch ist allerdings, dass die txt-Dateien nicht über vorhersagbare URLs erreichbar sind. (Als Beispiel: \url{https://www.bundestag.de/blob/398158/75976bb307d2f7cf7de345625a9c8692/18142-data.txt} ). Um sämtliche Protokolle herunterladen zu können, ist daher das Scraping der Webseite nötig. Erschwert wird dies dadurch, dass nicht alle Protokolle aller Wahlperioden auf der selben Seite aufgelistet werden, sondern einige ältere Protokolle nur auf einer Archivseite auffindbar sind. Trotz des höheren Aufwands beim Download haben wir uns für die txt-Version der Protokolle entschieden, da diese die weitere Verarbeitung vereinfachen sollten.

Es wurde in Python ein Crawler implementiert, der möglichst viele txt-Protokolle auf der Webseite des Bundestages findet und herunterlädt. Dabei wird darauf geachtet die Webseite nicht unnötig zu belasten, weshalb beispielsweise einmal heruntergeladene Protokolle bei erneuten Durchläufen nicht erneut heruntergeladen werden. Aktuell umfasst unsere Protokollsammlung alle Protokolle angefangen bei Wahlperiode 17 Sitzungstag 33 (24. März 2010) bis einschließlich Wahlperiode 19 Sitzungstag 13 (21. Februar 2018). Das sind zwar nicht alle Protokolle, aber da der Fokus von Bundestalk auf aktuellen politischen Themen liegt, reichen acht Jahre an Protokollen vorerst.

\subsection{Parsing}

Da die Suchmaschine einzelne Redebeiträge und nicht einfach das ganze Protokoll als Dokumente betrachten soll, ist es nötig die heruntergeladenen Protokolle zu parsen und in einzelne Redebeiträge zu zerlegen. Darüber hinaus sollen auch Metadaten erfasst werden, wie der aktuelle Sprecher und der zugehörige Tagesordnungspunkt.\\

Die Redeprotokolle haben alle den gleichen groben Aufbau. Zu Beginn steht ein Header mit Daten wie Wahlperiode, Sitzungstag, Ort und Datum.

Anschließend folgt das Inhaltsverzeichnis. Hier sind alle Tagesordnungspunkte mit den für den jeweiligen Tagesordnungspunkt geplanten Sprechern aufgelistet. Für die Tagespunkte namens Fragerunde sind auch alle geplanten Fragen und Fragesteller aufgelistet. Ferner sind auch alle Anlagen des Protokolls aufgelistet.

Schließlich folgt der Hauptteil, in dem nun sämtliche Redebeiträge der Sitzung aufgezählt werden.

Letztendlich folgen diverse Anhänge wie die Liste der entschuldigten Abgeordneten, etwaige Abstimmungsergebnisse, Anträge und Ähnliches. Die Anhänge werden von Bundestalk nicht erfasst und verarbeitet.\newline


Problematisch an den Protokollen ist, dass diese so gestaltet sind, dass sie für Menschen möglichst einfach lesbar sind. Auf die Maschinenlesbarkeit wird dabei kaum Rücksicht genommen. So wird beispielsweise der Beginn eines Redebeitrages durch den Namen des Sprechenden Politikers gefolgt von einem Doppelpunkt eingeleitet. Die Entscheidung, ob eine gegebene Zeichenfolge für den Namen eines Sprechers steht, ist für Computer äußerst schwierig. Glücklicherweise folgt auf Politikernamen in den Protokollen zumeist auch die Erwähnung der Partei des jeweiligen Politikers. Da es nur wenige Parteien im Bundestag gibt, erleichtert das die Erkennung erheblich. Allerdings gibt es von dieser Regel auch Ausnahmen, etwa bei Bundesministern und Staatssekretären, wofür der Parser diverse Sonderregeln implementieren muss. Außerdem fehlt in den Protokollen die Zuordnung, welcher Redebeitrag zu welchem Tagesordnungspunkt gehört. Beginn und Ende werden lediglich vom Bundestagspräsidenten angekündigt. Aufgrund der vielen möglich Formulierungen für eine solche Ankündigung ist eine maschinelle Erkennung kaum möglich (es wurde erfolglos versucht).\\

Das Hauptproblem sind allerdings die unzähligen kleinen Unterschiede in der Formatierung der verschiedenen Protokolle. Über die Dauer der letzten zwei Wahlperioden verändern sich laufend Teile der Protokollformatierung. Einige Beispiele für solche Änderungen sind:

\begin{itemize}  
    \item Einrückungen von Redebeiträgen
    \item Bedeutung von Zeilenumbrüchen
    \item verschiedene Zeichen für Einrückungen (tabs, spaces, sogar stellenwise Unicode BLANK-Characters)
    \item Sporadische trailing spaces (oder tabs, oder sonstige Unicodezeichen)
    \item Unterschiedliche Schreibweisen für z.B. den Titel Inhaltsverzeichnisses ("Inhalt","Inhalt:","I n h a l t:","I n h a l t :")
\end{itemize}

Am abwechslungsreichsten ist die Formatierung des Inhaltsverzeichnisses. Es gibt verschiedene Arten von Inhalten, die jeweils unterschiedliche Felder haben können, von denen einige auch gelegentlich unterschiedlich formatiert sind (z.B. Politikernamen über eine oder mehrere Zeilen) oder auch einfach komplett fehlen.
Da zudem keines der Felder explizit benannt ist, muss der Parser versuchen, aus dem Kontext zu erkennen, ob die gegebene Zeichenfolge nun der Titel, ein Sprecher, ein Anhang, eine Seitenangabe oder sonstiges ist.

Ebenfalls problematisch ist das Encoding der einzelnen Protokolle, da die Protokolle in einer Vielzahl verschiedener Zeichensätze vorliegen und der Zeichensatz des Dokumentes oft genug nicht mit dem vom Webserver angekündigten Zeichensatz übereinstimmt. Dazu kommen noch scheinbare Encoding-Probleme beim Erstellen der Protokolle. So tauchen beispielsweise Namen von Politikern, die spezielle Zeichen (Akzente o.ä.) enthalten in den unterschiedlichsten Schreibweisen auf. Stellenweise sogar mit Fragezeichen anstelle des Buchstabens (es handelt sich hierbei um echte ASCII-Fragezeichen im Originaldokument, nicht um Decodingfehler unsererseits).

Alles in allem ist anhand der vielen Variationen offensichtlich, dass die Formatierung händisch hergestellt wurde.\\

Es musste also ein Parser entwickelt werden, der alle Protokolle, trotz der massiven Variationen in der Formatierung in sinnvolle Redebeiträge zerlegen kann und zusätzlich fehlende Strukturinformationen (wie z.B. den aktuellen Tagesordnungspunkt) hinzufügt.
Die Scripte von OffenesParlament.de erwiesen sich als äußerst hilfreich. Viele der dort verwendeten regulären Ausdrücke zum Verarbeiten des Textes konnten wir als Ausgangspunkt verwenden. Allerdings mussten diese sehr oft angepasst werden, um alle in unserer Protokolsammlung (die umfangreicher ist als die von OffenesParlament.de) auftretenden Sonderfälle abzufangen.
Das Parsing geschieht grob in folgenden Schritten:

\begin{enumerate} 
\item Das Inhaltsverzeichnis wird geparst und die Tagesordnungspunkte, Sprecher und Metadaten extrahiert. Aufgrund der vielen verschiedenen Varianten, in denen das Inhaltsverzeichnis und seine Unterpunkte auftreten, ist ein Parsen mittels regulären Ausdrücken kaum möglich. Stattdessen kommt hier ein Recursive Descend Parser zum Einsatz.

\item Einzelne Aussagen und die dazugehörigen Sprecher werden mittels regulären Ausdrücken extrahiert. Diese Aussagen können Teile von Redebeiträgen, Zwischenrufe, Kundgebungen von Emotionen oder Moderationen des Bundestagspräsidenten sein. Der jeweilige Typ wird natürlich auch erfasst.

\item Die in 1. und 2. erhaltenen Daten werden kombiniert, (indem die Liste der geplanten Redner mit den protokollierten Rednern abgeglichen wird) um jeden Redebeitrag zum passenden Tagesordnungspunkt zuzuordnen.

\item Die extrahierten Daten werden in einer SQLite-Datenbank gespeichert.

\item Es werden HTML-Versionen der Protokolle erstellt, in die HTML-anchor eingefügt werden, um später im Webinterface an relevante Stellen springen zu können.
\end{enumerate}

So konnten letztendlich die Protokolle mit einer zufriedenstellenden Genauigkeit geparst werden. Dennoch verbleiben mit Sicherheit noch einige Fehler aufgrund ungewöhnlicher Sonderfälle, die bisher nicht aufgefallen sind. Außerdem sind aufgrund der ständig neu dazu kommenden Protokolle wahrscheinlich laufend Anpassungen am Parser nötig.
