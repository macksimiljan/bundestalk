\section{Evaluation}\label{Evaluation}
Zur Evaluation des Informationssystems wurden 14 Topics im Schema der \textit{Text Retrieval Conference} (TREC)\footnote{\url{http://trec.nist.gov/}} von drei Personen erstellt. Der Titel des jeweiligen Topics wurde als Suchanfrage verwendet. Syntaktisch lassen sich die Topics in die Kategorien Schlüs\-sel\-wort\-suche mit oder ohne Filter, Phrasensuche mit oder ohne Filter und einfaches Filtern von Dokumenten einteilen. Die durch ihre Suchanfrage repräsentierten Topics sind in Tab.~\ref{tab:topics} dargestellt. Die Hälfte aller Topic sind eine einfache Suchanfrage ohne Filter. 

Die ersten 20 Ergebnis-Dokumente pro Topic wurden von der Person bewertet, die das jeweilige Topic erstellt hatte. Zur Bewertung wurde eine ungeradzahlige Skala verwendet: \textit{irrelevant}, \textit{teilweise relevant}, \textit{relevant}. In der Berechnung der Korrektheit der Suchmaschine wurden alle Dokumente, die mit \textit{teilweise relevant} oder \textit{relevant} annotiert waren, als relevant gewertet.\footnote{Eine stärkere Einschränkung auf nur die Dokumente, die \textit{relevant} markiert wurden, führte zu vergleichbaren Resultaten.} Die Berechnung basiert auf \textit{Precision@k}, d.h. für jedes relevante Dokument im Ranking wird ein Precision-Wert berechnet. Die aggregierten Precision-Werte pro Topic sind in der letzten Spalte von Tab.~\ref{tab:topics} zu sehen. Es ergibt sich somit eine \textit{mean average precision} von $0,87$.

Die Precision-Werte machen einige Herausforderungen deutlich. Erstens, einige Topics, die häufig im Bundestag diskutiert worden sind, wie beispielsweise der Bundeswehreinsatz in Afghanistan oder die Infrastukturabgabe (ID 101, 300), erreichen sehr gute Precision-Werte, da es viele relevante Dokumente hierzu gibt. Bei einer solchen Suchanfrage wäre es interessanter, die Auswertung auf die top-$k$ Ergebnisse mit $k \gg 20$ auszudehnen und stärker die Vollständigkeit der Ergebnisliste zu betrachten. Ein Nutzer einer solch allgemeinen Suchanfrage könnte eine umfangreiche und vollständige Recherche zu diesem Thema zum Ziel haben. Zweitens, das Verzichten auf eine Phrasensuche bei \textit{Solar Valley} (106) führt zum einen dazu, dass auch Dokumente erscheinen, die \textit{valley} beinhalten, aber nichts mit Solar Valley in Sachsen-Anhalt zu tun haben. Zum anderen aber werden auch Redebeiträge zur Solarenergie gefunden, die nicht explizit Solar Valley erwähnen, aber die durchaus relevant sind. Drittens, Topic 202 ist insofern anspruchsvoll, als dass nicht nur nach einem Konzept gesucht wird, sondern nach zwei Konzepten die in einem bestimmten Zusammenhang stehen: \textit{Redebeiträge, in denen ein Vergleich zwischen Trump \emph{[Konzept 1]} und der AfD \emph{[Konzept 2]} gezogen wird.} Viertens, die Anfragen 207 und 208 zum Klimawandel zeigen ein bisher nicht behobenes Problem: Die beiden Filter zur Emotion und zur Partei, die die Emotion kundtut, sind mit Oder verknüpft, d.h. es werden Reden gesucht, die das Wort \textit{Klimawandel} beinhalten und als Reaktion Widerspruch hervorrufen oder eine beliebige Reaktion von den Grünen beinhalten. Aufgrund der hohen Auftretenswahrscheinlichkeit von Beifall als Emotion, wird weiterhin ein gutes Ergebnis erzielt. Bei der selteneren Widerspruch-Emotion führt dies allerdings zu einem schlechten Ergebnis. Fünftens, generell sollte bei mehr-wortigen Suchbegriffen die Phrasensuche verwendet werden, um ein gutes Ergebnis zu erzielen, da ansonsten die Wörter zu weit entfernt im Dokument oder unter Umständen gar nicht auftauchen. Teilweise kann dieses Verhalten durch weitere Filter kompensiert werden, wie Topic 201 und 205 zeigen.\footnote{Besonders im Beispiel \textit{Ehe für alle} ist, dass bei einfacher Stichwortsuche und Stoppwort-Eliminierung bei einer umfangreichen Stoppwort-Liste als Suche nur noch \textit{Ehe} auftaucht. Ohne Stoppwort-Eliminierung finden sich auch viele Dokumente weit oben, die nur \textit{für} und \textit{alle} beinhalten, weil diese Wörter sehr häufig sind.}


\begin{table}
	\caption{Evaluierte Topics nach syntaktischer Kategorie (SWS = Schlüsselwortsuche, PS = Phrasensuche, F = Filter) mit \textit{Average Precision} (AvgP).}
	\label{tab:topics}
	\renewcommand{\arraystretch}{1.5}
	\begin{tabular}{cllc}
		\toprule
		ID 	& Repräsentation der Suchanfrage 									& Kategorie & AvgP\\
		\midrule
		101 & \textit{Bundeswehr in Afghanistan}								& SWS & $1,00$ \\
		106 & \textit{Solar Valley}												& SWS & $0,65$ \\
		202 & \textit{Trump AfD}												& SWS & $0,64$ \\
		203 & \textit{Cannabis als Arzneimittel}								& SWS & $0,91$ \\
		204 & \textit{Finanzierung verfassungsfeindlicher Parteien}				& SWS & $0,79$ \\
		300 & \textit{Infrastrukturabgabe (Ausländermaut)}						& SWS & $1,00$\\
		301 & \textit{Incirlik}													& SWS & $1,00$ \\
		105 & \textit{Friedliche Revolution} + Beifall von CDU/CSU				& SWS mit F & $0,83$ \\
		207 & \textit{Klimawandel} + Widerspruch von Grünen						& SWS mit F & $0,51$ \\
		208 & \textit{Klimawandel} + Beifall von Grünen							& SWS mit F & $0,90$ \\
		209 & \textit{Einwanderung} + von: AfD + (Zuruf | Widerspruch | Unruhe) & SWS mit F & $1,00$ \\
		201 & \textit{Ehe für alle} + 30.06.17						& SWS mit F & $0,96$ \\
		205 & \glqq \textit{Ehe für alle}\grqq									& PS & $1,00$ \\
		206 & von: AfD + Emotion von Linken										& F & $0,97$ \\
		\bottomrule
	\end{tabular}
	\renewcommand{\arraystretch}{1}
\end{table}

Ein Vergleich von \textit{Bundestalk} mit den bereits vorhandenen Suchmaschinen gestaltet sich schwierig, da ein anderer Dokumentbegriff verwendet wird und weniger Filter vorhanden sind. So ermöglicht OffenesParlament beispielsweise keine Phrasensuche. Das bedeutet, dass das implementierte Informationssystem ein sachkundigeres Suchen und ein auf die Suchparameter stärker fokussiertes Ergebnis bietet. Eine Auswertung von OffenesParlament wurde unter folgenden Annahmen vorgenommen. Erstens, es wurde wohlwollend gewertet, d.h. es wurde geschaut, ob das verlinkte Protokoll einen für die Suchanfrage relevanten Abschnitt enthält. Zweitens, für Topic 105 wurde auf die Einschränkungen verzichtet, dass der Abschnitt Beifall von der CDU/CSU enthalten soll. Drittens, andere Topics mit Emotionen wurden nicht mehr verglichen. Viertens, das Topic zu \textit{Solar Valley} wurden ebenfalls nicht ausgewertet, da die relevanten Dokumente aufgrund ihres Datums nicht im Datensatz von OffenesParlament enthalten sind. Es zeigt sich ein schlechteres Ergebnis für die \textit{average precision}, siehe Tab.~\ref{tab:comparison}. Weiterhin zeigt die Tabelle in der letzten Spalte, dass sich die implementiere Suchmaschine u.a. durch das Einführen von Stemming weiterhin verbessern konnte.

\begin{table}
\caption{\textit{Average Precision} von \textit{OffenesParlament} und \textit{Bundestalk}.}
\label{tab:comparison}
\renewcommand{\arraystretch}{1.3}
\centering
\begin{tabular}{cccc}
\toprule
Topic & OffenesParlament & Bundestalk & Bundestalk (nach Stemming)\\
\midrule
		101 & $0,60$ & $1,00$ & $1,00$\\
		106 & -- & $0,65$ & $0,79$ \\
		202 & $1,00$ & $0,64$ & $0,73$ \\
		203 & $1,00$ & $0,91$ & $0,96$ \\
		204 & $0,68$ & $0,79$ & $0,91$ \\
		300 & $0,78$ & $1,00$ & $1,00$ \\
		301 & $1,00$ & $1,00$ & $0,97$ \\
		105 & $0,41$ & $0,83$ & $0,82$ \\
		207 & $0,33$ & $0,51$ & $0,52$ \\
		208 & -- & $0,90$ & $1,00$ \\
		209 & -- & $1,00$ & $1,00$ \\
		201 & $0,00$ & $0,96$ & $1,00$ \\
		205 & $0,00$ & $1,00$ & $1,00$ \\
		206 & -- & $0,97$ & $1,00$  \\
		\midrule
		AVG & $0,58$ & $0,86$ & $0,91$\\
\bottomrule
\end{tabular}
\renewcommand{\arraystretch}{1.5}
\end{table}
