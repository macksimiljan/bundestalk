import json
from functools import reduce

from mean_average_precision import mean_average_precision
from mean_average_precision import average_precision_distribution
from mean_average_precision import precisions_at_rank
from mean_average_precision import average_precision

with open('relevant.json', 'r') as f:
    judgements = json.load(f)

threshold = 1

ratings_list = list(map(lambda j: j['ratings'], judgements))
offenes_parlament_list = list(map(lambda j: j['offenes_parlament'], judgements))
print(offenes_parlament_list)
print('#topics:', len(ratings_list))
print('#ratings:', reduce(lambda x, y: x + y, map(lambda list: len(list), ratings_list)))
print('mean avg precision:', mean_average_precision(ratings_list, 1))
average_precision_distribution(ratings_list, 1, list(map(lambda j: j['topic_id'], judgements)))

precisions_list = list(map(lambda ratings: precisions_at_rank(ratings, threshold), ratings_list))
avg_precisions = list(map(lambda pair: average_precision(pair[0], pair[1], threshold), zip(precisions_list, ratings_list)))
print(list(zip(map(lambda p: round(p, 2), avg_precisions), list(map(lambda j: j['topic_id'], judgements)))))

# for offenes_parlament
precisions_list = list(map(lambda ratings: precisions_at_rank(ratings, threshold), offenes_parlament_list))
avg_precisions = list(map(lambda pair: average_precision(pair[0], pair[1], threshold), zip(precisions_list, offenes_parlament_list)))
print('offenes parlament: ')
print(list(zip(map(lambda p: round(p, 2), avg_precisions), list(map(lambda j: j['topic_id'], judgements)))))
print('mean avg precision:', mean_average_precision(offenes_parlament_list, 1))

threshold = 2
print('increasing threshold to 2')

precisions_list = list(map(lambda ratings: precisions_at_rank(ratings, threshold), ratings_list))
avg_precisions = list(map(lambda pair: average_precision(pair[0], pair[1], threshold), zip(precisions_list, ratings_list)))
print(list(zip(map(lambda p: round(p, 2), avg_precisions), list(map(lambda j: j['topic_id'], judgements)))))


# after update of SE
print('\nafter update of SE')
with open('relevantsV2.json', 'r') as f:
    judgements = json.load(f)

threshold = 1

ratings_list = list(map(lambda j: j['ratings'], judgements))
precisions_list = list(map(lambda ratings: precisions_at_rank(ratings, threshold), ratings_list))
avg_precisions = list(map(lambda pair: average_precision(pair[0], pair[1], threshold), zip(precisions_list, ratings_list)))
print(list(zip(map(lambda p: round(p, 2), avg_precisions), list(map(lambda j: j['topic_id'], judgements)))))
print('mean avg precision:', mean_average_precision(ratings_list, 1))

