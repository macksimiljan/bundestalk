/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package groupa;

import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;

/**
 * Index Klasse
 * stellt den File Index zur verfügung
 */
public class index {
    public Analyzer analyzer;
    public Directory index;
    
    public index(String indexDirectoryPath) throws IOException {
        analyzer = new org.apache.lucene.analysis.de.GermanAnalyzer(CharArraySet.EMPTY_SET);
        index = FSDirectory.open(java.nio.file.Paths.get(indexDirectoryPath));
        
    }
    
    public void clearIndex() throws IOException{
        IndexWriterConfig  config = new IndexWriterConfig(analyzer);
        IndexWriter indexWriter = new IndexWriter(index, config);
        indexWriter.deleteAll();
        indexWriter.close();
    }
    
    
}