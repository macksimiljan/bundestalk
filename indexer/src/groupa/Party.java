package groupa;

import java.util.ArrayList;


public class Party {
    private String slug;
    private String name;

    public Party(String slug, String name) {
        this.slug = slug;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
    public String getSlug() {
        return this.slug;
    }
}
