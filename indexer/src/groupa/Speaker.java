package groupa;

import java.util.ArrayList;


public class Speaker {
    private String name;
    private String fingerprint;
    private String party;

    public Speaker(String name, String fingerprint, String party) {
        this.name = name;
        this.fingerprint = fingerprint;
        if(party != null) this.party = "media/parteien/" + party + ".png";
        else this.party = "";
    }

    public String getName() {
        return this.name;
    }
    public String getFingerprint() {
        return this.fingerprint;
    }
    public String getParty(){
        return this.party;
    }
}
