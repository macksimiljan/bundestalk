/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package groupa;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.HashMap;
import java.util.ArrayList;
import groupa.Speaker;
import groupa.Party;

/**
 *
 * @author falk
 */
public class Suggester {
    
    Connection conn;
    
    public Suggester(String sqlFileFilePath) throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        conn = DriverManager.getConnection("jdbc:sqlite:" + sqlFileFilePath);
    }
    
    /**
     * Bildet Tabelle mit allen Dateien
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public void buildParties() throws IOException, ClassNotFoundException, SQLException {
        System.out.println("groupa.Suggester.buildParties()");
        
        Statement stmt = conn.createStatement();
        stmt.execute( "CREATE TABLE IF NOT EXISTS parties ("
                + " slug VARCHAR(50) PRIMARY KEY,"
                + " name VARCHAR(100)"
                + " )" );
        
        stmt.execute( "DELETE FROM parties" );
        
        ResultSet rs = stmt.executeQuery( "SELECT speaker_party"
                + " FROM speak"
                + " WHERE NOT IFNULL(speaker_party, ':') like '%:%'"
                + " GROUP BY speaker_party" );
        
        while ( rs.next() ) {
            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO parties (slug, name) VALUES (?,?)");
            pstmt.setString(1, rs.getString("speaker_party"));
            pstmt.setString(2, getPartyName(rs.getString("speaker_party")));
            pstmt.executeUpdate();
        }
    }
    
    /**
     * Gibt Dateinamen zu Slug zurück
     * Analog zu /crawler/module/normdatei/parties.py
     * @param PartySlug
     * @return 
     */
    private String getPartyName(String PartySlug){
        switch (PartySlug) {
            case "cducsu": return "CDU/CSU";
            case "spd": return "SPD";
            case "linke": return "Die Linke";
            case "fdp": return "FDP";
            case "gruene": return "BÜNDNIS 90/DIE GRÜNEN";
            case "afd": return "AfD";
            default: return PartySlug;
        }
    }
    
    public ArrayList<Party> SearchParties(String value) throws IOException, ClassNotFoundException, SQLException{
        ArrayList<Party> res = new ArrayList<>(); 

        String sql = "SELECT slug, name"
                + " FROM parties"
                + " WHERE name like ? OR slug like ?"
                + " ORDER BY name";
        
         PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, value + "%");
        pstmt.setString(2, value + "%");
         
        ResultSet rs = pstmt.executeQuery();
        while ( rs.next() ) {
            res.add(new Party(rs.getString("slug"), rs.getString("name")));
        }
        
        return res;
    }

    public ArrayList<Speaker> searchSpeaker(String nameStart) throws IOException, ClassNotFoundException, SQLException{
        ArrayList<Speaker> res = new ArrayList<>();

        String sql = "SELECT fingerprint, name, party "
            + "FROM speakers "
            + "WHERE name LIKE ? "
            + "ORDER BY name ";

        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, "%" + nameStart + "%");

        ResultSet speakers = pstmt.executeQuery();
        while (speakers.next())
            res.add(new Speaker(speakers.getString("name"), speakers.getString("fingerprint"), speakers.getString("party")));

        return res;
    }

    public List<String> searchEmotion(String substring) throws SQLException {
        String sql = "SELECT DISTINCT name" +
            "         FROM emotions" +
            "         WHERE name IS NOT NULL" +
            "           AND name LIKE ? ;";

        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, "%" + substring + "%");

        ResultSet emotions = preparedStatement.executeQuery();
        List<String> result = new ArrayList<>();
        while (emotions.next()) {
            String emotion = emotions.getString("name");
            result.add(emotion);
        }

        return result;
    }
    
}