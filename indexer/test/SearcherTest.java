import groupa.doc;
import groupa.searchResult;
import groupa.searcher;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SearcherTest {

  private static searcher searcher;
  private Integer offset = 0;
  private Integer limit = 5;

  private HashMap<String, String> searchParams;

  @BeforeClass
  public static void setUp() throws Exception {
    String indexDirectoryPath = "../data/index/";
    searcher = new searcher(indexDirectoryPath, null);
  }

  @Before
  public void init() {
    offset = 0;
    limit = 5;
    searchParams = new HashMap<>();
  }

  @Test
  public void testSearchSimple() {
    try {
      searchParams.put("text", "maut");
      searchResult result = searcher.boolSearch(searchParams, offset, limit);
      assertEquals(5, result.docs.size());

      offset = 6;
      limit = 13;
      result = searcher.boolSearch(searchParams, offset, limit);
      assertEquals(13, result.docs.size());
    } catch (IOException | ParseException | InvalidTokenOffsetsException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testSearchNonSense() {
    searchParams.put("text", "ölxy1vtasf");
    try {
      searchResult result = searcher.boolSearch(searchParams, offset, limit);
      assertEquals(0, result.count);
    } catch (IOException | ParseException | InvalidTokenOffsetsException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testSearchAdvancedScenario1() {
    searchParams.put("party", "spd");
    searchParams.put("wp", "18");
    searchParams.put("text", "maut");
    searchParams.put("speaker", "Sebastian Hartmann");
    try {
      searchResult result = searcher.boolSearch(searchParams, offset, limit);
      assertEquals(5, result.docs.size());
    } catch (IOException | ParseException | InvalidTokenOffsetsException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testSearchAdvancedScenario2() {
    searchParams.put("bTop", "Afghanistan");
    try {
      searchResult result = searcher.boolSearch(searchParams, offset, limit);
      assertEquals(5, result.docs.size());
    } catch (IOException | ParseException | InvalidTokenOffsetsException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testSearchAdvancedScenario3() {
    searchParams.put("text", "maut");
    searchParams.put("timestamp", "1418857200-1514329200");
    try {
      searchResult result = searcher.boolSearch(searchParams, offset, limit);
      assertEquals(5, result.docs.size());
    } catch (IOException | ParseException | InvalidTokenOffsetsException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testSearchAdvancedScenario4() {
    searchParams.put("emotion", "beifall");
    searchParams.put("emotion_party", "linke");
    searchParams.put("text", "maut");
    try {
      searchResult result = searcher.boolSearch(searchParams, offset, limit);
      assertEquals(5, result.docs.size());
    } catch (IOException | ParseException | InvalidTokenOffsetsException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testSearchAdvancedScenario5() {
    searchParams.put("emotion", "beifall");
    try {
      searchResult result = searcher.boolSearch(searchParams, offset, limit);
      assertEquals(5, result.docs.size());
    } catch (IOException | ParseException | InvalidTokenOffsetsException e) {
      fail(e.getMessage());
    }
  }
}
