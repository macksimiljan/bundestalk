import groupa.Party;
import groupa.Speaker;
import groupa.Suggester;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * sudo ant test
 */
public class SuggesterTest {
  private static Suggester suggester;

  @BeforeClass
  public static void setUp() throws SQLException, ClassNotFoundException {
    String databasePath = "../data/db/db.sqlite";
    suggester = new Suggester(databasePath);
  }

  @Test
  public void testSearchParties() {
    try {
      ArrayList<Party> result = suggester.SearchParties("f");
      assertEquals(1, result.size());
      assertEquals("fdp", result.get(0).getSlug());

      result = suggester.SearchParties("");
      assertEquals(6, result.size());
    } catch (IOException | ClassNotFoundException | SQLException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testSearchSpeaker() {
    try {
      List<Speaker> result = suggester.searchSpeaker("angela");
      assertEquals(2, result.size());
      boolean containsAngelaMerkel = false;
      for (Speaker speaker : result)
        containsAngelaMerkel |= speaker.getFingerprint().equals("angela-merkel");
      assertTrue(containsAngelaMerkel);

      result = suggester.searchSpeaker("");
      assertTrue(result.size() > 1000);
    } catch (IOException | ClassNotFoundException | SQLException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testSearchEmotion() {
    try {
      List<String> result = suggester.searchEmotion("ruf");
      assertThat(result, is(Arrays.asList("Gegenruf", "Zuruf")));
    } catch (SQLException e) {
      fail(e.getMessage());
    }

  }
}